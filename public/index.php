<?php

use Phalcon\Config, 
    Phalcon\Config\Adapter\Ini, 
    Phalcon\Mvc\Application, 
    Phalcon\DI\FactoryDefault ;

try 
{
    $config = new Config() ;
    $config->merge( new Ini( __DIR__.'/../common/config/databases.ini' ) ) ;
    
    $di = new FactoryDefault() ;
    
    include __DIR__.'/../common/config/services.php' ;
    include __DIR__.'/../common/config/routes.php' ;
    include __DIR__.'/../common/config/cache.php' ;
    
    // Create an application
    $application = new Application( $di ) ;
    
    // Register the installed modules
    $application->registerModules( require __DIR__.'/../common/config/modules.php' ) ;
    
    // Handle the request
    echo $application->handle()->getContent() ;
} 
catch( \Exception $e )  
{
    echo $e->getMessage() ;
}