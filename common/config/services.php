<?php

use Phalcon\Db\Adapter\Pdo\Mysql, 
    Phalcon\Db\Adapter\Pdo\Postgresql ;

// Set MySQL
$di->set( 'MySQL', function() use ( $config ) {
    return new Mysql( array( 
        'host' => $config->MySQL->host
    ) ) ;
} ) ;

$di->set( 'PostgreSQL', function() use ( $config ) {
    return new Postgresql( array(
        'host'      => $config->PostgreSQL->host, 
        'username'  => $config->PostgreSQL->username, 
        'password'  => $config->PostgreSQL->password, 
        'dbname'    => $config->PostgreSQL->dbname
    ) ) ;
} ) ;

