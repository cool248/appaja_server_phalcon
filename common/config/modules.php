<?php

return array( 
    'dialog' => array( 
        'className' => 'Appaja\API\Dialog\Module', 
        'path'      => '../apps/dialog/Module.php'
    ), 
    'language' => array(
        'className' => 'Appaja\API\Language\Module', 
        'path'      => '../apps/language/Module.php'
    ), 
    'location' => array(
        'className' => 'Appaja\API\Location\Module', 
        'path'      => '../apps/location/Module.php'
    ), 
    'module' => array( 
        'className' => 'Appaja\API\Module\Module', 
        'path'      => '../apps/module/Module.php'
    ), 
    'stop' => array(
        'className' => 'Appaja\API\Stop\Module',
        'path'      => '../apps/stop/Module.php'
    ), 
    'tip' => array(
        'className' => 'Appaja\API\Tip\Module', 
        'path'      => '../apps/tip/Module.php'
    ), 
    'transportation' => array(
        'className' => 'Appaja\API\Transportation\Module', 
        'path'      => '../apps/transportation/Module.php'
    ),
    'user' => array(
        'className' => 'Appaja\API\User\Module',
        'path'      => '../apps/user/Module.php'
    ),
    'reportTransportation' => array(
        'className' => 'Appaja\API\reportTransportation\Module',
        'path'      => '../apps/reportTransportation/Module.php'
    )
    
) ;