<?php

use Phalcon\Mvc\Router ;

// Specify routes for modules
$di->set( 'router', function() {
    $router = new Router() ;
    $router->setDefaultAction( 'index' ) ;
    $router->add( '/:module/:controller/:action', array( 
        'module'        => 1, 
        'controller'    => 2, 
        'action'        => 3, 
    ) ) ;

    return $router ;
} ) ;
