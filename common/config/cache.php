<?php

use Phalcon\Cache\Frontend\Data, 
  Phalcon\Cache\Backend\Memcache ;

$di->set( 'modelsCache', function() {
    // Cache data for one day by default
    //86400
    $frntCch = new Data( array( 'lifetime' => 86400 ) ) ;
    // Memcache connection settings
    $cch = new Memcache( $frntCch, array( 
        'host' => 'phalcon-memcache.cuqbad.0001.apse1.cache.amazonaws.com', 'port' => '11211'
    ) ) ;
    

    return $cch ;
} ) ;