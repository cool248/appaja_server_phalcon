<?php

namespace Appaja\API\User\Library ;

class UserUtils 
{
    private $user_id = 0;
    private $validate_id = 0;
    private $status = 0;
    
    public function set_status($s)
    {
        $this->status = $s;
    }
    
    public function get_status()
    {
        $s = $this->_checkUserStatus();
        $this->set_status($s);
        return $this->status;
    }
    
    public function set_userId($u)
    {
        $this->user_id = $u;
    }
    
    public function get_userId()
    {
        return $this->user_id;
    }
    
    public function set_validateId($v)
    {
        $this->validate_id = $v;
    }
    
    public function get_validateId()
    {
        return $this->validate_id;
    }
    
   
}
