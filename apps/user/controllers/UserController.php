<?php

namespace Appaja\API\User\Controllers ;

use Phalcon\Db\Column,
    Phalcon\Mvc\Controller,
    Phalcon\DI,
    Appaja\API\User\Models\Users,
    Appaja\API\User\Models\UsersDevice,
    Appaja\API\User\Library\UserUtils,
    Appaja\API\User\Models\Provider;    

class UserController extends Controller
{
    public function indexAction()
    {
        $result = $this->checkDeviceCodeAction(4321);
        print_r($result->count());
    }
    
    public function checkAndRegisterAction()
    {
        $userDeviceId = $this->request->getQuery('user_device_id');
        $validateId = $this->request->getQuery('validate_id');
        $fullName = $this->request->getQuery('full_name');
        $twitter_username = $this->request->getQuery('twitter_username');
        $gender = $this->request->getQuery('gender');
        $country = $this->request->getQuery('country');
        $language = $this->request->getQuery('language');
        $profileImageUrl = $this->request->getQuery('profile_image_url');
        $email = $this->request->getQuery('email');
        $provider = $this->request->getQuery('provider');
        
        $data  = array();
        $data['status'] = false;
                
        
        $user = Users::find(array(
            "conditions" => "validate_id = ?1",
            "bind" => array(1 => "0")
        ));
        
        
        if(count($user) != 0)
        {
            /**
            *      USER AVAILABLE
            **/
        }else
        {
            /**
            *   USER NOT AVALAIBLE
            **/
            
        }
        exit();
        $data['status'] = "true";
        if($user->validate_id == $validateId)
        {

            $data['message'] = "registered user";
            
            /**
            *    just retreive the data
            */
        }else
        {
            $data['message'] = "user not yet registered, so let's register our new friend";
            /**
            *   register the user
            */
            $user->validate_id = $validateId;
            $user->twitter_username = $twitter_username;
            $user->full_name = $fullName;
            $user->registered = 1;
            $user->gender = $gender;
            $user->country = $country;
            $user->language = $language;
            $user->profile_image_url = $profileImageUrl;
            $user->email = $email;
            $provider = Provider::findFirst(array(
                "conditions" => "provider_name = ?1",
                "bind" => array(1 => $provider)
            ));
            $user->provider = $provider->id;
            
        }
        
        if($user->save())
        {
             $data['message'] = "user successfully registered";
            
        }else
        {
             $data['message'] = "user not registered";
             
              foreach ($user->getMessages() as $message) {
                $data['message'] = $data['message'].",".$message;
            }
    
    
            
        }
        
        $provider = Provider::findFirst(array(
                "conditions" => "id = ?1",
                "bind" => array(1 => $user->provider)
            ));

            $data['provider'] = $provider->provider_name;
            $data['full_name'] = $user->full_name;
       echo json_encode($data);
    }
    
    
    /**
    *   check if the device code exists and return or create new and return
    */
    public function getUsersDeviceIdAction()
    {
        $data = array();
        $deviceCode = $this->request->getQuery( 'deviceCode') ;
        $deviceId = $this->request->getQuery( 'deviceId') ;
        $result = UsersDevice::findFirst(array(
            "conditions" => "device_code = ?1",
            "bind" => array(1 => $deviceCode)
        ));
        
        if(!empty($result))
        {
            $user = Users::findFirst(array(
                "conditions" => "id = ?1",
                "bind" => array(1 => $result->users_id)
            ));
            
            $data['status'] = true;
            $data['userDeviceId'] = $result->id;
            $data['nick_name'] = $user->full_name;
            $data['newUser'] = false;
        }else
        {
            $data['status'] = true;
            /**
            *   create new user and get the user device id
            **/
            $newUser = new Users();
            $newUser->full_name = 'anonymous';
            $newUser->registered = 0;
            $newUser->create();
            $data['userId'] = $newUser->id;
            
            $newUsersDeviceId = new UsersDevice();
            $newUsersDeviceId->users_id = $newUser->id;
            $newUsersDeviceId->device_code = $deviceCode;
            $newUsersDeviceId->device_id = $deviceId;
            $newUsersDeviceId->create();
            
            $data['userDeviceId'] = $newUsersDeviceId->id;
            $data['nick_name'] = 'anonymous';
            $data['newUser'] = true;
        }        
        
        echo json_encode($data);
    }
  
    
    
    public function getByUserIdAction()
    {
        $userId = $this->request->getQuery('user_id');
        $data = array();
        if(is_null($userId))
        {
            $data['status'] = false;
            $data['message'] = "user id is empty";
            echo json_encode($data);
            exit();
        }

        $user = Users::findFirst(array(
            "conditions" => "id = ?1",
            "bind" => array(1 => $userId)
        ));        
        
        if(is_object($user))
        {
            $data['status'] = true;
            $data['message'] = "user avalaible";
            $data['dt'] = $user->toArray();
        }
        echo json_encode($data);
    }
    
    /**
    *   API CREATE ACCOUNT
    */
    public function createAccountAction(){
        /**
        *    just generate a anonymous account
        *   RETURN ::
        *   user_id(mandatory)
        */
        
        $deviceId = $this->request->getQuery('device_id');
        $userId = $this->_createAccount($deviceId);
        $user = Users::findFirst(array(
            "conditions" => "id = ?1",
            "bind" => array(1 => $userId)
            ));
            
        $data = array();
        $data['status'] = true;
        $data['dt'] = $user;
        echo json_encode($data);
    }
    
    
    /***
    *   API REGISTER ACCOUNT
    */
    public function registerAccountAction(){
        /**
        *    check if the validated id exists in other user id
        *   if not then just update with current user id
        *    else 
        *   use the previous user id which already exists
        *   RETURN ::
        *   user_id(mandatory)
        */
        $data = array();
        $validateId = $this->request->getQuery('validate_id');
        $userId = $this->request->getQuery('user_id');

            /*         Some import data         */
/*=================================================================*/


/*=================================================================*/
        $user = Users::findFirst(array(
            "conditions" => "validate_id = ?1",
            "bind" => array(1 => $validateId)
        ));
        
        if(is_object($user))
        {
            /**
            *   validate id exits
            **/
            $data['status'] = true;
            $data['old_user'] = true;
            $data['dt'] = $user;
            $data['message'] = "you are already an appajer's, let's get you ready!!";
            
        }else
        {
           $data = $this->_registerationProcess($userId,array(),$validateId);
        }
        
        echo json_encode($data);
    }
    
    /**
    *   $user_id = id of the user
    *   $userData = (array) of user data
    *
    */
    public function _registerationProcess($userId,$userData,$validateId)
    {
        $data = array();
         $user = Users::findFirst(array(
            "conditions" => "id = ?1",
            "bind" => array(1 => $userId)
            ));
            
                $user->validate_id = $validateId;
                $user->full_name = $userData['fullName'];
                $user->registered = 1;
                $user->twitter_username = $userData['twitter_username'];
                $user->email = $userData['email'];
                $user->gender = $userData['gender'];
                $user->country = $userData['country'];
                $user->language = $userData['language'];
                $user->profile_image_url = $userData['profileImageUrl'];

                $provider = Provider::findFirst(array(
                "conditions" => "provider_name = ?1",
                "bind" => array(1 => $userData['provider'])
            ));


                $user->provider = $provider->id;
                $status = $user->update();
                
                $data['status'] = $status;
                
                if($status)
                {
                    $data['dt'] = $user->toArray();
                    
                }else
                {
                    foreach ($robot->getMessages() as $message) {
                        $data['message'] = $data['message'] + $message;
                    }
                }
            return $data;
    }
    
    public function loginAction()
    {        
        $data = array();
        $userData = array();
        $validateId = $this->request->getQuery('validate_id');
        $userId = $this->request->getQuery('user_id');
        $deviceId = $this->request->getQuery('device_id');
        $userData['fullName'] = $this->request->getQuery('full_name');
        $userData['twitter_username'] = $this->request->getQuery('display_name');
        $userData['email'] = $this->request->getQuery('email');
        $userData['gender'] = $this->request->getQuery('gender');
        $userData['country'] = $this->request->getQuery('country');
        $userData['language'] = $this->request->getQuery('language');

        $userData['profileImageUrl'] = $this->request->getQuery('profile_image_url');
        $userData['provider'] = $this->request->getQuery('provider');
        
        if($validateId == "" || $validateId == 0)
        {
            $data['status'] = false;
            $data['message'] = "validate id cannot be empty or 0";
            
            echo json_encode($data);
            exit();
        }
        
        $user = Users::findFirst(array(
            "conditions" => "validate_id = ?1",
            "bind" => array(1 => $validateId)
        ));
        
        if(is_object($user))
        {
            /**
            *   user exits
            **/
            $data['status'] = true;
            $data['old_user'] = true;
            $data['dt'] = $user;
            $data['message'] = "You are loggedin,".$userData['fullName'];
            
        }else
        {
            /**
            *   Create Account
            *   Register Account
            */
            if($userId == null)
            {
                $userId = $this->_createAccount($deviceId);
                
            }
            
            $data = $this->_registerationProcess($userId,$userData,$validateId);            
            
            $data['old_user'] = false;
            $data['message'] = "welcome to Appaja,".$userData['fullName'];
        }
/*         echo $userData['provider'];exit(); */
            $data['dt']->provider = $userData['provider'];
        echo json_encode($data);
        
    }
    
    
    
    /**
    *    return user_id
    */
    public function _createAccount($deviceId)
    {
        $newUser = new Users();
        $newUser->full_name = 'anonymous';
        $newUser->registered = 0;
        $newUser->create();
        $userId = $newUser->id;
        
        $newUsersDeviceId = new UsersDevice();
        $newUsersDeviceId->users_id = $userId;
        $newUsersDeviceId->device_code = 0;
        $newUsersDeviceId->device_id = $deviceId;
        $newUsersDeviceId->create();        
        
        return $userId;
    }
}
