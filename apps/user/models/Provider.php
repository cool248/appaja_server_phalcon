<?php
namespace Appaja\API\User\Models ;

use Phalcon\Mvc\Model;

class Provider extends Model 
{    
    /**
     * Initialize
     */
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships
        $this->hasMany('id','Users','id');
    }
    
}