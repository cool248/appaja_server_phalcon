<?php
namespace Appaja\API\User\Models ;

use Phalcon\Mvc\Model,
    Phalcon\Mvc\Model\Behavior\Timestampable,
    Appaja\API\reportTransportation\Models\UserReportsTraject ;  

class Users extends Model 
{    
    /**
     * Initialize
     */
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships
/*         $this->belongsTo( 'stp', 'Stps', 'id' ) ; */
        $this->hasMany('id','UsersDevice','users_id');
        $this->belongsTo( 'provider', 'provider', 'id' ) ;
        $this->hasMany('id','UserReportsTraject','user_id');
        
        date_default_timezone_set("Asia/Bangkok");
         $this->addBehavior(new Timestampable(array(
            'beforeValidationOnCreate' => array(
                'field' => 'created_at',
                'format' => 'Y-m-d H:i:s'
            )
        )));
    }
    
}