<?php
namespace Appaja\API\User\Models ;

use Phalcon\Mvc\Model ;

class UsersDevice extends Model 
{    
    /**
     * Initialize
     */
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships (many to one)
        $this->belongsTo( 'users_id', 'users', 'id' ) ;
        $this->belongsTo( 'device_id', 'device', 'id' ) ;
                
        // Define relationships (one to many)
        $this->hasMany('id','StpRprts','id_users_device');
    }
    
}