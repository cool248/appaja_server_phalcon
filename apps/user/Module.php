<?php

namespace Appaja\API\User;

use Phalcon\Loader, 
    Phalcon\Mvc\Dispatcher, 
    Phalcon\Mvc\ModuleDefinitionInterface, 
    Phalcon\Mvc\View ;

class Module implements ModuleDefinitionInterface 
{
    
    /**
     * Register a specific autoloader for the module
     */
    public function registerAutoloaders()
    {
        $loader = new Loader() ;
        $loader->registerNamespaces( array(
            'Appaja\API\User\Controllers' => 
                '../apps/user/controllers/', 
            'Appaja\API\User\Models' => 
                '../apps/user/models/',
            'Appaja\API\User\Library' => '../apps/user/library/' 
        ) )->register() ;
    }
    
    public function registerServices( $di )
    {
        // Register a dispatcher
        $di->set( 'dispatcher', function() {
            $dispatcher = new Dispatcher() ;
            $dispatcher->setDefaultNamespace( 
                'Appaja\API\User\Controllers' ) ;
            
            return $dispatcher ;
        } ) ;
        
        $di->set( 'view', function() {
            $view = new View() ;
            $view->setViewsDir( '../apps/user/views/' ) ;
            
            return $view ;
        } ) ;
    }
    
}
        