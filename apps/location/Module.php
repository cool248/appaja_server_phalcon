<?php

namespace Appaja\API\Location ;

use Phalcon\Loader, 
    Phalcon\Mvc\Dispatcher, 
    Phalcon\Mvc\ModuleDefinitionInterface, 
    Phalcon\Mvc\View ;

class Module implements ModuleDefinitionInterface 
{
    
    /**
     * Register a specific autoloader for the module
     */
    public function registerAutoloaders()
    {
        $loader = new Loader() ;
        $loader->registerNamespaces( array(
            'Appaja\API\Language\Models' => '../apps/language/models', 
            'Appaja\API\Location\Controllers' => '../apps/location/controllers/', 
            'Appaja\API\Location\Models' => '../apps/location/models/', 
            'Appaja\API\Transportation\Models' => '../apps/transportation/models'
        ) )->register() ;
    }

    /**
     * Register services
     */
    public function registerServices( $di )
    {
        $di->set( 'dispatcher', function() {
            $dispatcher = new Dispatcher() ;
            $dispatcher->setDefaultNamespace( 'Appaja\API\Location\Controllers' ) ;
            
            return $dispatcher ;
        } ) ;
        
        $di->set( 'view', function() {
            $view = new View() ;
            $view->setViewsDir( '../apps/location/views' ) ;
            
            return $view ;
        } ) ;
    }
    
}
