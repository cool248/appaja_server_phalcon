<?php

namespace Appaja\API\Location\Models ;

use Phalcon\Mvc\Model, 
    Appaja\API\Language\Models\Lnggs, 
    Appaja\API\Location\Models\Stts ;

class SttDtls extends Model
{
    
    public function initialize()
    {
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        $this->belongsTo( 'stt', 'Stts', 'id' ) ;
        $this->belongsTo( 'lngg', 'Lnggs', 'id' ) ;
    }
    
}

