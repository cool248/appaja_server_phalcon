<?php

namespace Appaja\API\Location\Models ;

use Phalcon\Mvc\Model, 
    Appaja\API\Language\Models\Lnggs, 
    Appaja\API\Location\Models\Cntrs ;

class CntryDtls extends Model
{
    
    public function initialize()
    {
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        $this->belongsTo( 'cntry', 'Cntrs', 'id' ) ;
        $this->belongsTo( 'lngg', 'Lnggs', 'id' ) ;
    }
    
}

