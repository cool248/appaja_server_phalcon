<?php

namespace Appaja\API\Location\Models ;

use Phalcon\Mvc\Model, 
 Appaja\API\Location\Models\Sbdstrcts, 
 Appaja\API\Location\Models\VllgDtls, 
 Appaja\API\Transportation\Models\TrjctRtsVllgs ;

class Vllgs extends Model
{
    
    public function initialize()
    {
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        $this->hasMany( 'id', 'VllgDtls', 'vllg' ) ;
        $this->hasMany( 'id', 'TrjctRtsVllgs', 'vllg' ) ;
        $this->belongsTo( 'sbdstrct', 'Sbdstrcts', 'id' ) ;
    }
    
}

