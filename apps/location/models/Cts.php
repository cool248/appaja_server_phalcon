<?php

namespace Appaja\API\Location\Models ;

use Phalcon\Mvc\Model, 
    Appaja\API\Location\Models\CtyDtls, 
    Appaja\API\Location\Models\Stts ;

class Cts extends Model
{
    
    public function initialize()
    {
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        $this->hasMany( 'id', 'CtyDtls', 'cty' ) ;
        $this->belongsTo( 'stt', 'Stts', 'id' ) ;
    }
    
}

