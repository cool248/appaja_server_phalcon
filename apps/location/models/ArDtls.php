<?php

namespace Appaja\API\Location\Models ;

use Phalcon\Mvc\Model, 
 Appaja\API\Language\Models\Lnggs, 
 Appaja\API\Location\Models\Ars ;

class ArDtls extends Model
{
    
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships
        $this->belongsTo( 'ar', 'Ars', 'id' ) ;
        $this->belongsTo( 'lngg', 'Lnggs', 'id' ) ;
    }
    
}