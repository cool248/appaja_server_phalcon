<?php

namespace Appaja\API\Location\Models ;

use Phalcon\Mvc\Model, 
 Appaja\API\Language\Models\Lnggs, 
 Appaja\API\Location\Models\Vllgs ;

class VllgDtls extends Model
{
    
    public function initialize()
    {
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        $this->belongsTo( 'vllg', 'Vllgs', 'id' ) ;
        $this->belongsTo( 'lngg', 'Lnggs', 'id' ) ;
    }
    
}

