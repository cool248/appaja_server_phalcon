<?php

namespace Appaja\API\Location\Models ;

use Phalcon\Mvc\Model, 
    Appaja\API\Location\Models\Cntrs, 
    Appaja\API\Location\Models\Cts, 
    Appaja\API\Location\Models\SttDtls ;

class Stts extends Model
{
    
    public function initialize()
    {
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        $this->hasMany( 'id', 'SttDtls', 'stt' ) ;
        $this->hasMany( 'id', 'Cts', 'stt' ) ;
        $this->belongsTo( 'cntry', 'Cntrs', 'id' ) ;
    }
    
}

