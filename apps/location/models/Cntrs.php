<?php

namespace Appaja\API\Location\Models ;

use Phalcon\Mvc\Model, 
    Appaja\API\Location\Models\CntryDtls, 
    Appaja\API\Location\Models\Stts ;

class Cntrs extends Model
{
    
    public function initialize()
    {
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        $this->hasMany( 'id', 'CntryDtls', 'cntry' ) ;
        $this->hasMany( 'id', 'Stts', 'cntry' ) ;
    }
    
}

