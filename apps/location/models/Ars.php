<?php

namespace Appaja\API\Location\Models ;

use Phalcon\Mvc\Model, 
    Appaja\API\Location\Models\ArDtls, 
    Appaja\API\Transportation\Models\TrjctRtsArs ;

class Ars extends Model
{
    
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships
        $this->hasMany( 'id', 'ArDtls', 'ar' ) ;
        $this->hasMany( 'id', 'TrjctRtsArs', 'ar' ) ;
    }
    
}

