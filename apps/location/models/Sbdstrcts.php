<?php

namespace Appaja\API\Location\Models ;

use Phalcon\Mvc\Model,  
    Appaja\API\Location\Models\Cts, 
    Appaja\API\Location\Models\SbdstrctDtls, 
    Appaja\API\Location\Models\Vllgs, 
    Appaja\API\Transportation\Models\TrjctRtsSbdstrcts ;

class Sbdstrcts extends Model
{
    
    public function initialize()
    {
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        $this->hasMany( 'id', 'Vllgs', 'sbdstrct' ) ;
        $this->hasMany( 'id', 'SbdstrctDtls', 'sbdstrct' ) ;
        $this->hasMany( 'id', 'TrjctRtsSbdstrcts', 'sbdstrct' ) ;
        $this->belongsTo( 'cty', 'Cts', 'id' ) ;
    }
    
}