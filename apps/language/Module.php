<?php

namespace Appaja\API\Language ;

use Phalcon\Loader, 
    Phalcon\Mvc\Dispatcher, 
    Phalcon\Mvc\ModuleDefinitionInterface, 
    Phalcon\Mvc\View ;

class Module implements ModuleDefinitionInterface 
{
    
    /**
     * Register a specific autoloader for the module
     */
    public function registerAutoloaders()
    {
        $loader = new Loader() ;
        $loader->registerNamespaces( array(
            'Appaja\API\Language\Controllers' => '../apps/language/controllers/', 
            'Appaja\API\Dialog\Models' => '../apps/dialog/models/', 
            'Appaja\API\Language\Models' => '../apps/language/models/', 
            'Appaja\API\Location\Models' => '../apps/location/models/', 
            'Appaja\API\Module\Models' => '../apps/module/models', 
            'Appaja\API\Stop\Models' => '../apps/stop/models', 
            'Appaja\API\Tip\Models' => '../apps/tip/models', 
            'Appaja\API\Transportation\Models' => 
                '../apps/transportation/models' ) )->register() ;
    }

    /**
     * Register services
     */
    public function registerServices( $di )
    {
        $di->set( 'dispatcher', function() {
            $dispatcher = new Dispatcher() ;
            $dispatcher->setDefaultNamespace( 'Appaja\API\Language\Controllers' ) ;
            
            return $dispatcher ;
        } ) ;
        
        $di->set( 'view', function() {
            $view = new View() ;
            $view->setViewsDir( '../apps/language/views' ) ;
            
            return $view ;
        } ) ;
    }
    
}
