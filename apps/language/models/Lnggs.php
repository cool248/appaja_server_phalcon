<?php

namespace Appaja\API\Language\Models ;

use Phalcon\Mvc\Model, 
    Appaja\API\Dialog\Models\DlgDtls, 
    Appaja\API\Location\Models\ArDtls, 
    Appaja\API\Location\Models\CntryDtls, 
    Appaja\API\Location\Models\CtyDtls, 
    Appaja\API\Location\Models\SbdstrctDtls, 
    Appaja\API\Location\Models\SttDtls, 
    Appaja\API\Stop\Models\StpDtls,
    Appaja\API\Stop\Models\StpTypDtls, 
    Appaja\API\Tip\Models\TpCtgryDtls, 
    Appaja\API\Transportation\Models\TrjctRtTypDtls, 
    Appaja\API\Transportation\Models\TrnsprttnCmpnyDtls, 
    Appaja\API\Transportation\Models\TrnsprttnTypDtls ;

class Lnggs extends Model
{
    
    /**
     * Initialize
     */
    public function initialize()
    {
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        $this->hasMany( 'id', 'ArDtls', 'lngg' ) ;
        $this->hasMany( 'id', 'CntryDtls', 'lngg' ) ;
        $this->hasMany( 'id', 'CtyDtls', 'lngg' ) ;
        $this->hasMany( 'id', 'DlgDtls', 'lngg' ) ;
        $this->hasMany( 'id', 'SbdstrctDtls', 'lngg' ) ;
        $this->hasMany( 'id', 'StpDtls', 'lngg' ) ;
        $this->hasMany( 'id', 'StpTypDtls', 'lngg' ) ;
        $this->hasMany( 'id', 'SttDtls', 'lngg' ) ;
        $this->hasMany( 'id', 'TpCtgryDtls', 'lngg' ) ;
        $this->hasMany( 'id', 'TpDtls', 'lngg' ) ;
        $this->hasMany( 'id', 'TrjctRtTypDtls', 'lngg' ) ;
        $this->hasMany( 'id', 'TrnsprttnCmpnyDtls', 'lngg' ) ;
        $this->hasMany( 'id', 'TrnsprttnTypDtls', 'lngg' ) ;
    }
    
}

