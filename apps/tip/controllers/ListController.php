<?php

namespace Appaja\API\Tip\Controllers ;

use Phalcon\Mvc\Controller, 
    Appaja\API\Tip\Models\Tps, 
    Appaja\API\Tip\Models\TpDtls ;

class ListController extends Controller
{
    
    public function indexAction()
    {
        $tps = array() ;
        
        $this->tps_st( $tps ) ;
        
        echo json_encode( array( 'errr' => 0, 'dt' => array( 'tps' => $tps ) ) ) ;
    }
    
    public function getAllAction()
    {
        $data = array();
        $tips = Tps::find( array( 
            'cache' => array( 'key' => 'tips_all' ),
            'order' => 'id asc' 
        ) ) ;
        
        foreach($tips as $t)
        {

            $tipsDetailEnglish = TpDtls::find( array( 
                'cache' => array( 'key' => 'tips_dtls_english_all'.$t->id ),
                'conditions' => "tp = ?1 and lngg = ?2",
                'bind' => array(1=>$t->id, 2 => 1)
            ) )->toArray() ;
            
            $tipsDetailIndonesia = TpDtls::find( array( 
                'cache' => array( 'key' => 'tips_dtls_indonesia_all'.$t->id ),
                'conditions' => "tp = ?1 and lngg = ?2",
                'bind' => array(1=>$t->id, 2 => 2)
            ) )->toArray() ;
            


            $data['data'][] = array(
                "id" => $t->id,
                "category"=>$t->ctgry,
                "sequence" => $t->sqnc,
                "en" => $tipsDetailEnglish[0]['txt'],
                "in" => $tipsDetailIndonesia[0]['txt']
            );
            
            //$tipsDetail = null;
        }
        
        echo json_encode($data);
    }
    
    private function tps_st( &$tps )
    {
        $cndtns = 'ctgry = '.$_GET[ 'ctgry' ] ;
        $tps_rslt_st = Tps::find( array( 
            'cache' => array( 'key' => 'tps_w_ctgry'.$_GET[ 'ctgry' ] ),
            'conditions' => $cndtns, 
            'order' => 'sqnc' 
        ) ) ;
        
        $ii = 0 ;
        while( $tps_rslt_st->valid() )
        {
            $tp_rcrd = $tps_rslt_st->current() ;
            $tps[ $ii ][ 'id' ] = $tp_rcrd->id ;
            
            $cndtns = 'tp = '.$tp_rcrd->id ;
            $dtls_rslt_st = TpDtls::find( array( 
                'cache' => array( 'key' => 'tp_dtls_w_tp'.$tp_rcrd->id ),
                'conditions' => $cndtns
            ) ) ;
            $jj = 0 ;
            while( $dtls_rslt_st->valid() )
            {
                $dtl_rcrd = $dtls_rslt_st->current() ;
                
                if( $jj == 0 ) 
                {
                    $tps[ $ii ][ 'en-us' ] = $dtl_rcrd->txt ;
                }
                else
                {
                    $tps[ $ii ][ 'id-id' ] = $dtl_rcrd->txt ;
                }
                
                $jj++ ;
                $dtls_rslt_st->next() ;
            }
            
            $ii++ ;
            $tps_rslt_st->next() ;
        }
    }
    
}

