<?php

namespace Appaja\API\Tip\Controllers ;

use Phalcon\Mvc\Controller,
    Appaja\API\Tip\Models\TpCtgrs, 
    Appaja\API\Tip\Models\TpCtgryDtls ;

class CategoriesController extends Controller
{
    
    /**
     * Action index 
     */
    public function indexAction()
    {
        $ctgrs = array() ;
        
        $this->ctgrs_st( $ctgrs ) ;
        
        echo json_encode( $ctgrs ) ;
    }
    
    /**
     * Set categories
     * @param array The categories
     */
    private function ctgrs_st( &$ctgrs )
    {
        $ctgrs_rslt_st = TpCtgrs::find( array( 
            'cache' => array( 'key' => 'tp_ctgrs' ), 
            'order' => 'sqnc' ) ) ;
        
        $ii = 0 ;
        while( $ctgrs_rslt_st->valid() )
        {
            $rcrd = $ctgrs_rslt_st->current() ;
            $ctgrs[ $ii ][ 'id' ] = $rcrd->id ;
            
            $cndtns = 'ctgry = '.$rcrd->id.' AND lngg = '.$_GET[ 'lngg' ] ;
            $dtls_rslt_st = TpCtgryDtls::find( array( 
                'cache' => array( 'key' => 'tp'.$rcrd->id.'_ctgry_dtls-lngg'.$_GET[ 'lngg' ] ),
                'conditions' => $cndtns
            ) ) ;
            
            while( $dtls_rslt_st->valid() )
            {
                $dtl_rcrd = $dtls_rslt_st->current() ;
                $ctgrs[ $ii ][ 'txt' ] = $dtl_rcrd->nm ;
                
                $dtls_rslt_st->next() ;
            }
            
            $ii++ ;
            $ctgrs_rslt_st->next() ;
        }
    }
    
}
