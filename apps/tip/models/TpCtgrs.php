<?php

namespace Appaja\API\Tip\Models ;

use Phalcon\Mvc\Model, 
 Appaja\API\Tip\Models\TpCtgryDtls, 
 Appaja\API\Tip\Models\Tps ;

class TpCtgrs extends Model
{
    
    /**
     * Initialize 
     */
    public function initialize()
    {
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        $this->hasMany( 'id', 'TpCtgryDtls', 'ctgry' ) ;
        $this->hasMany( 'id', 'Tps', 'ctgry' ) ;
    }
    
}
