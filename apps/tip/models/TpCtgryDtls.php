<?php

namespace Appaja\API\Tip\Models ;

use Phalcon\Mvc\Model, 
 Appaja\API\Language\Models\Lnggs, 
 Appaja\API\Tip\Models\TpCtgrs ;

class TpCtgryDtls extends Model
{
    
    public function initialize()
    {
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        $this->belongsTo( 'ctgry', 'TpCtgrs', 'id' ) ;
        $this->belongsTo( 'lngg', 'Lnggs', 'id' ) ;
    }
    
}

