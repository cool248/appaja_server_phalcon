<?php

namespace Appaja\API\Tip\Models ;

use Phalcon\Mvc\Model, 
 Appaja\API\Tip\Models\TpDtls ;

class Tps extends Model
{
    
    /**
     * Initialize
     */
    public function initialize()
    {
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        $this->hasMany( 'id', 'TpDtls', 'tp' ) ;
    }
    
}

