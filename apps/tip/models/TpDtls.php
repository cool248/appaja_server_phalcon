<?php

namespace Appaja\API\Tip\Models ;

use Phalcon\Mvc\Model, 
 Appaja\API\Language\Models\Lnggs, 
 Appaja\API\Tip\Models\Tps ;

class TpDtls extends Model
{
    
    /**
     * Initialize
     */
    public function initialize()
    {
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        $this->belongsTo( 'tp', 'Tps', 'id' ) ;
        $this->belongsTo( 'lngg', 'Lnggs', 'id' ) ;
    }
    
}
