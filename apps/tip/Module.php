<?php

namespace Appaja\API\Tip ;

use Phalcon\Loader, 
    Phalcon\Mvc\Dispatcher, 
    Phalcon\Mvc\ModuleDefinitionInterface, 
    Phalcon\Mvc\View ;

class Module implements ModuleDefinitionInterface 
{
    
    /**
     * Register a specific autoloader for the module
     */
    public function registerAutoloaders()
    {
        $loader = new Loader() ;
        $loader->registerNamespaces( array( 
            'Appaja\API\Language\Models' => '../apps/language/models', 
            'Appaja\API\Tip\Controllers' => '../apps/tip/controllers/', 
            'Appaja\API\Tip\Models' => '../apps/tip/models/'
        ) )->register() ;
    }
    
    public function registerServices( $di )
    {
        // Register a dispatcher
        $di->set( 'dispatcher', function() {
            $dispatcher = new Dispatcher() ;
            $dispatcher->setDefaultNamespace( 'Appaja\API\Tip\Controllers' ) ;
            
            return $dispatcher ;
        } ) ;
        
        $di->set( 'view', function() {
            $view = new View() ;
            $view->setViewsDir( '../apps/tip/views/' ) ;
            
            return $view ;
        } ) ;
    }
    
}
        