<?php

namespace Appaja\API\Stop\Models ;

use Phalcon\Mvc\Model,
    Phalcon\Mvc\Model\Behavior\Timestampable ;

class StpRprts extends Model 
{    
    /**
     * Initialize
     */
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships
        $this->belongsTo( 'stp', 'Stps', 'id' ) ;
        $this->belongsTo( 'id_users_device', 'UsersDevice', 'id' ) ;
        $this->belongsTo( 'id_user', 'Users', 'id' ) ;
        
        date_default_timezone_set("Asia/Bangkok");
         $this->addBehavior(new Timestampable(array(
            'beforeValidationOnCreate' => array(
                'field' => 'time_stamp',
                'format' => 'Y-m-d H:i:s'
            )
        )));
    }
    
}