<?php

namespace Appaja\API\Stop\Models ;

use Phalcon\Mvc\Model;

class Color extends Model 
{
    /**
     * Initialize
     */
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
        
    }
}