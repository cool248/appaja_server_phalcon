<?php
namespace Appaja\API\Stop\Models ;

use Phalcon\Mvc\Model ;

class UsersDevice extends Model 
{    
    /**
     * Initialize
     */
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships (many to one)
        $this->belongsTo( 'users_id', 'users', 'id' ) ;
        $this->belongsTo( 'device_id', 'device', 'id' ) ;
                

    }
    
}