<?php
namespace Appaja\API\Stop\Models ;

use Phalcon\Mvc\Model ;

class Device extends Model 
{    
    /**
     * Initialize
     */
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships
        $this->hasMany( 'id', 'UsersDevice', 'device_id' ) ;
    }
    
}