<?php

namespace Appaja\API\Stop\Models ;

use Phalcon\Mvc\Model, 
    Appaja\API\Language\Models\Lnggs,
    Appaja\API\Stop\Models\Stps ;

class StpDtls extends Model 
{
    
    /**
     * Initialize
     */
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships
        $this->belongsTo( 'stp', 'Stps', 'id' ) ;
        $this->belongsTo( 'lngg', 'Lnggs', 'id' ) ;
    }
    
}

