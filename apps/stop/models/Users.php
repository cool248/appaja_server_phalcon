<?php
namespace Appaja\API\Stop\Models ;

use Phalcon\Mvc\Model ;

class Users extends Model 
{    
    /**
     * Initialize
     */
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships
        $this->belongsTo( 'stp', 'Stps', 'id' ) ;
        $this->hasMany('id','UsersDevice','users_id');
        // Define relationships (one to many)
        $this->hasMany('id','StpRprts','id_user');
    }
    
}