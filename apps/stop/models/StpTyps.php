<?php

namespace Appaja\API\Stop\Models ;

use Phalcon\Mvc\Model, 
    Appaja\API\Stop\Models\StpTypDtls, 
    Appaja\API\Stop\Models\Stps ;

/**
 * Stop types model
 */
class StpTyps extends Model
{
    
    /**
     * Initialize
     */
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships
        $this->hasMany( 'id', 'StpTypDtls', 'typ' ) ;
        $this->hasMany( 'id', 'Stps', 'typ' ) ;
    }
    
}

