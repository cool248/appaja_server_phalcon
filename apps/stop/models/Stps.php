<?php

namespace Appaja\API\Stop\Models ;

use Phalcon\Mvc\Model, 
    Appaja\API\Stop\Models\StpDtls, 
    Appaja\API\Stop\Models\StpTyps ;

/**
 * Stops model
 */
class Stps extends Model
{
    
    public $id ;
    
    public function getSource()
    {
        return 'stps' ;
    }
    
    /**
     * Initialize
     */
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships
        $this->hasMany( 'id', 'StpDtls', 'stp' ) ;
        $this->belongsTo( 'typ', 'StpTyps', 'id' ) ;
    }
    
}



