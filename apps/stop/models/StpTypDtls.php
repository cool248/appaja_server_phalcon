<?php

namespace Appaja\API\Stop\Models ;

use Phalcon\Mvc\Model, 
    Appaja\API\Language\Models\Lnggs, 
    Appaja\API\Stop\Models\StpTyps ;

/**
 * Stop type details model
 */
class StpTypDtls extends Model
{
    
    /**
     * Initialize
     */
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships
        $this->belongsTo( 'typ', 'StpTyps', 'id' ) ;
        $this->belongsTo( 'lngg', 'Lnggs', 'id' ) ;
    }
    
}