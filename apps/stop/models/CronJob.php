<?php

namespace Appaja\API\Stop\Models ;

use Phalcon\Mvc\Model;

class CronJob extends Model 
{
    /**
     * Initialize
     */
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
    }
}