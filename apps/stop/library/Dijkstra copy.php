<?php

namespace Appaja\API\Stop\Library ;

class Dijkstra {
    
    /**
     * Calculate
     * @param type $graph_array
     * @param type $source
     * @param type $target
     * @return array
     */
    public static function clc( $graph_array, $source, $target ) 
    {
    
        $vertices = array() ;
        $neighbours = array() ;

/**
make the data structured so that we can use it easily
*/
        foreach( $graph_array as $edge ) 
        {
            array_push( $vertices, $edge[ 'stp_frm' ], $edge[ 'stp_t' ] ) ;

            $neighbours[ $edge[ 'stp_frm' ] ][] = array( "end" => $edge[ 'stp_t' ], 
                "cost" => $edge[ 'cst' ], "id_line" => $edge[ 'trjct' ] );
        }
        
/*         print_r(json_encode($neighbours));exit(); */


        /*** The code below removes the duplicate results ***/
        $vertices = array_unique( $vertices ) ;
/*         print_r(json_encode($vertices)); */

/**
    make 2 array for holding the visited and unvisited vertex
    dist = unvisited = set the value as infinity
    previous = visited
*/
        foreach( $vertices as $vertex ) 
        {
            $dist[ $vertex ] = INF ;
            $previous[ $vertex ] = NULL;
        }
        
        

/**
    set the source value as 0
*/
        $dist[ $source ] = 0 ;
        $Q = $vertices ;


        while( count( $Q ) > 0 ) 
        {

            // TODO - Find faster way to get minimum

            /*
             *   FIRST FIND THE SMALLEST DISTANCE IN THE $dist array();
             *   ALSO THE NODE;
             */
            $min = INF;
            foreach ($Q as $vertex) {
                if ($dist[$vertex] < $min) {
                    $min = $dist[$vertex]; /* MINIMUM DISTANCE */
                    $u = $vertex; /* NODE */
                }
            }
            
            /**
                remove the current vertices from the array
            */    
            $Q = array_diff($Q, array($u));

            if ($dist[$u] == INF or $u == $target) {
                break;
            }
            
            /* print_r($neighbours[$u]);exit(); */
            
            /**
                if the current vertices is 33 then find all the neighbours of the 33
            */
           /*
 echo "<br/>";
            echo "<br/>";
            echo "u = ".$u;

            echo "<br/>";
            print_r($neighbours[$u]);
            echo "<br/>";
*/
            if (isset($neighbours[$u])) {
                foreach ($neighbours[$u] as $arr) {
                    $alt = $dist[$u] + $arr["cost"];
                  //  echo "alt = ".$alt;
                   // echo "<br/>";
                    if ($alt < $dist[$arr["end"]]) {
                        $dist[$arr["end"]] = $alt;
                        $previous[$arr["end"]] = $u;
                    }
                }
            }
        }
        
        $path = array();
        $u = $target;
                
        /**
            retrieve the proper order.
        */
        while (isset($previous[$u])) {
            array_unshift($path, $u);
            $u = $previous[$u];
        }
        
        /**
            since the source is not yet insert because it is not yet set (previous[$u] = null) we just add it.
        */
/*         array_unshift($path, $u); */
        
/*         print_r(json_encode($neighbours)); */
/*         echo "<br/>"; */
/*         echo "<br/>";          */
/*         print_r(json_encode($path)); */
/*         exit(); */


        $last_position = count($path) - 1;
        /**
            GETTING THE CORRECT LINE
        */
        $lines = array();
        
        for($i = 0;$i<count($path);$i++)
        {
            /**
            *   $next = halte berikutnya
            */
            $next = $i + 1;

            foreach($neighbours[$path[$i]] as $a)
            {
                if($next == count($path))
                {
                    break;
                }
                
                if($path[$next] == $a["end"])
                {
                    $lines[$path[$i]][] =  $a["id_line"];
                    //echo "<br/>";
                }
                            
            }
        }
       
       
        $correctLines = array();
        $i = 0;
        $previousLine = null;
        foreach($lines as $line)
        {
            //sort the array
            asort($line);
/*             print_r($line); */
/*             echo "</br>"; */
            
/*             echo reset($line); */
/*             echo "</br>"; */
/*             echo "</br>"; */
            $correctLines[$i] = reset($line);
            $currentLine = reset($line);
            if($currentLine != $previousLine && $previousLine != null)
            {
                foreach($line as $l)
                {
                   // echo "<->current line is = ".$currentLine;
                    //echo "||";
                    //echo "previous line is = ".$previousLine;
                    //echo "||";
                    //echo $l;
                    if($previousLine == $l)
                    {
                        //echo "oh we have the same here";
                        $correctLines[$i] = $l;
                        //break;
                    }
                }
            }
 
            //get the first position from array
            
            $i++;
            $previousLine = $currentLine;
        }
/*         print_r(json_encode($correctLines)); */

        
        /**
            print the perfect line to take
        */
/*         $i = 0; */
       /*
 foreach($lines as $line)
        {
         print_r($line[0]); 
         echo "<br/>";
         
         $next = $i+1;
         
         echo $path[$i]."=>".$path[$next]." naik ".$line[0];
         $i++;
       }
*/
        $result[ "path" ] = $path ;
        $result[ "line" ] = $correctLines ;

        return $result ;
    }

}

