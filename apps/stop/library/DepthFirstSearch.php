<?php

namespace Appaja\API\Stop\Library ;
use Phalcon\Db\Column, 
    Phalcon\Mvc\Model\Resultset, 
    Appaja\API\Stop\Models\Stps, 
    Appaja\API\Stop\Models\StpDtls;

class DepthFirstSearch {
    
    /**
    *   GLOBAL VARIBALE
    */
    public static $track = array();
    public static $index = 0;
    public static $_hop = 0;
    public static $MAX_HOP = 20;
    public static $MAX_RUTE = 10;
    
    public function search($start,$end,$vertex, $list)
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '4096M');
        self::$track[self::$index][] = $start;
        $this->depthFirstSearch($start,$end,$vertex, $list);
        $this->printTrack();
    }
    
    /**
     * Depth-first search of the graph
     * 
     * @param type $vertex Currently checked graph's vertex
     * @param type $list Incidence list of graph vertexes
     * @return Incidence list of graph vertexes
     */
     
    function depthFirstSearch($start,$end,$vertex, $list)
    {
    
        if (!$vertex['visited']) {
            /*
    echo "visited vertex = ".$vertex['letter']; // output on screen
            echo "</br>";
            echo "</br>";
    */
    
            // mark vertex as visited
            $list[$vertex['vertex']]['visited'] = true;
            
            
            foreach ($vertex['neighbours'] as $neighbour) {
    
                
                /*
    echo "current node is = ".$vertex['letter'];
                echo "</br>";
                echo "previous node is = ".$vertex['previous'];
                echo "</br>";
    */
    
                if($vertex['letter'] == $end)
                {
                    /*
    echo "</br>";
                    echo "Match Found";
                    echo "</br>";
    */
    
                /**
                *   back trace
                */
                
                }else
                {
     /*
                    echo "from vertex = ".$vertex['letter'];
                     echo "</br>";
                     echo "visiting vertex = ".$list[$neighbour]['letter'];
                    echo "</br>";
                    echo "</br>";
    */
                    
                    if($list[$neighbour]['letter'] == $end)
                    {
    /*                    echo "Bingo MATCH FOUND"; */
    
                       /*add the current node*/
                       $this->addNode($list[$neighbour]['letter']);
                       if($this->getCurrentIndex() == self::$MAX_RUTE)
                       {
    /*
                            echo "this is the end";
                            echo "<br/>";
                            echo "<br/>";
                            echo "<br/>";
    echo "<br/>";
    echo "<br/>";
    */
                           // $total = count(self::$track);
                            //self::$track['total'] = $total;
                            //print_r(json_encode(self::$track));
                            //exit();
                            $this->printTrack();
                       }
    
                       /*increment the track index*/
                       self::$index++;
    
                       /*copy all the previous track data to current track*/
                       self::$track[self::$index] = self::$track[self::$index-1];
    /*                    echo "current index ".self::$index; */
                  
                        /*Remove node*/
                        $this->removeNode($list[$neighbour]['letter']);
                       
                    }else
                    {
                        // Watch neighbours, which were not visited yet
                        if (!$list[$neighbour]['visited']) {
                            
                            
                           
                            if(self::$_hop < self::$MAX_HOP)
                            {
                            //add the previous vertex
                            $list[$neighbour]['previous'] = $vertex['letter'];
                            /*add node to track array*/
                            $this->addNode($list[$neighbour]['letter']);
                            
                             /*
                            *   increase hop
                            */
                                self::$_hop++;
    /*                             echo "</br>"; */
    /*                             echo "the hop is = ".self::$_hop; */
    /*                             echo "</br>"; */
                                // going through neighbour-vertexes
                                $list = $this->depthFirstSearch(
                                    $start,
                                    $end,
                                    $list[$neighbour], 
                                    $list
                                );
                            }else
                            {
    /*                             echo "</br>"; */
    /*                             echo "The hop has execced"; */
                            }
    /*                         //echo "hop is = ".self::$_hop; */
                            //echo "</br>";
                            
                        }else
                        {
                            /*
    echo "vertex is visited = ".$list[$neighbour]['letter'];
                            echo "</br>";
    */
                        }
                    }
                    
                }   
            }
            /*
    echo "<br/>";
            echo "exiting from this vertex = ".$vertex['letter'];
            echo "<br/>";
    */
            /*change the visited to false*/
            $list[$vertex['vertex']]['visited'] = false;
            /*remove the current node from track array*/
            $this->removeNode($vertex['letter']);
    /*         echo "<br/>"; */
            /**
            *   decrease hop
            */
            self::$_hop--;
    /*         echo "</br>"; */
    /*         echo "hop is reducing = ".self::$_hop; */
    /*         echo "</br>"; */
        }
         
    /*      print_r(self::$track); */
        return $list;
    }

    function getCurrentTrack()
    {
        return self::$track[$this->getCurrentIndex()];
    }


    function getCurrentIndex()
    {
        return self::$index;
    }
    
    function removeNode($nodeToRemove)
    {
        $key = array_keys(self::$track[self::$index],$nodeToRemove);
    /*     print_r($key); */
        unset(self::$track[self::$index][$key[0]]);
    /*     print_r($this->getCurrentTrack()); */
    }
    
    function addNode($nodeToAdd)
    {
        self::$track[self::$index][] = $nodeToAdd;
        
        //print_r($this->getCurrentTrack());exit();
    }
    
    public function printTrack()
    {
    
        $result = array();
    /*     $i = 0; */
        foreach(self::$track as $key => $value)
        {
    /*         print_r($value); */
    /*         echo "</br>"; */
    /*         $i++; */
            foreach($value as $stp)
            {
                $stop = StpDtls::findFirst(array(
                    "conditions" => "stp = ?1 and lngg = ?2",
                    "bind" => array(1 => $stp,2=>1)
                ));
    /*             echo $stp; */
    /*             echo "</br>"; */
    /*             echo $stop->nm; */
    /*             echo "</br>"; */
                $result[$key][] = $stop->nm;
            }
        }
        
        print_r(json_encode($result));exit();
    }
    
    function getGraph($graph_array)
    {
        $vertices = array();
        $neighbours = array();
        foreach($graph_array as $edge)
        {
            array_push( $vertices, $edge[ 'stp_frm' ], $edge[ 'stp_t' ] ) ;
        }
        
        $vertices = array_unique( $vertices ) ;
        
        foreach($vertices as $v)
        {
            $stops = StpDtls::findFirst(array(
                "conditions" => "stp = ?1 and lngg = ?2",
                "bind" => array(1 => $v,2 => 1)
            ));
            
            $neighbours[$v] = array(
                    'vertex' => $v,//vertex number
                    'visited' => false, // `visited` flag
                    'letter' => $v, // vertex value
                    'neighbours' => array(),
                    'previous'=> '',
                    'name' => $stops->nm
        
                    );
        }
        
    
        
    
        foreach($graph_array as $edge)
            {
        
                if(empty($neighbours[$edge['stp_frm']]))
                {
                    $neighbours[$edge['stp_frm']] = array(
                    'vertex' => $edge['stp_frm'],//vertex number
                    'visited' => false, // `visited` flag
                    'letter' => $edge['stp_frm'], // vertex value
                    'neighbours' => array($edge['stp_t']), // neighbours
                    'previous'=> ''
        
                    );
                    
                }else
                {
        
                    if(!in_array($edge['stp_t'],$neighbours[$edge['stp_frm']]['neighbours']))
                    {
                        array_push($neighbours[$edge['stp_frm']]['neighbours'],$edge['stp_t']); 
                    }
                    
                }
                
            }
    /*             print_r(json_encode($neighbours));exit(); */
            
            return $neighbours;
    }

}