<?php

namespace Appaja\API\Stop\Library ;

class DateUtils 
{    
    public static function dateDiffInMinute($startDate,$endDate)
    {
        date_default_timezone_set("Asia/Bangkok");
        if($startDate == null)
        {
            $start_date = new \DateTime(date('Y-m-d H:i:s'));
        }else
        {
            $start_date = new \DateTime($startDate);
        }
        

        $since_start = $start_date->diff(new \DateTime($endDate));
        
        $minutes = $since_start->days * 24 * 60;
        $minutes += $since_start->h * 60;
        $minutes += $since_start->i;
        return $minutes;
    }
}
