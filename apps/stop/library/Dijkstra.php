<?php

namespace Appaja\API\Stop\Library ;

class Dijkstra {
    
    public static $correctLines = array();
    /**
     * Calculate
     * @param type $graph_array
     * @param type $source
     * @param type $target
     * @return array
     */
    public static function clc( $graph_array, $source, $target ) 
    {

        $vertices = array() ;
        $neighbours = array() ;
        $value = 'drtn';
/**
make the data structured so that we can use it easily
*/
        foreach( $graph_array as $edge ) 
        {
            array_push( $vertices, $edge[ 'stp_frm' ], $edge[ 'stp_t' ] ) ;

            $neighbours[ $edge[ 'stp_frm' ] ][] = array( "end" => $edge[ 'stp_t' ], 
                "cost" => $edge[ $value ], "id_line" => $edge[ 'trjct' ] );
        }
        
/*         print_r(json_encode($neighbours));exit(); */


        /*** The code below removes the duplicate results ***/
        $vertices = array_unique( $vertices ) ;
/*         print_r(json_encode($vertices)); */

/**
    make 2 array for holding the visited and unvisited vertex
    dist = unvisited = set the value as infinity
    previous = visited
*/
        foreach( $vertices as $vertex ) 
        {
            $dist[ $vertex ] = INF ;
            $previous[ $vertex ] = NULL;
        }
        
        

/**
    set the source value as 0
*/
        $dist[ $source ] = 0 ;
        $Q = $vertices ;


        while( count( $Q ) > 0 ) 
        {

            // TODO - Find faster way to get minimum

            /*
             *   FIRST FIND THE SMALLEST DISTANCE IN THE $dist array();
             *   ALSO THE NODE;
             */
            $min = INF;
            foreach ($Q as $vertex) {
                if ($dist[$vertex] < $min) {
                    $min = $dist[$vertex]; /* MINIMUM DISTANCE */
                    $u = $vertex; /* NODE */
                }
            }
            
            /**
                remove the current vertices from the array
            */    
            $Q = array_diff($Q, array($u));

            if ($dist[$u] == INF or $u == $target) {
                break;
            }
            
   
            if (isset($neighbours[$u])) {
                foreach ($neighbours[$u] as $arr) {
                    $alt = $dist[$u] + $arr["cost"];
                  //  echo "alt = ".$alt;
                   // echo "<br/>";
                    if ($alt < $dist[$arr["end"]]) {
                        $dist[$arr["end"]] = $alt;
                        $previous[$arr["end"]] = $u;
                    }
                }
            }
        }
        
        $path = array();
        $u = $target;
                
        /**
            retrieve the proper order.
        */
        while (isset($previous[$u])) {
            array_unshift($path, $u);
            $u = $previous[$u];
        }
        /**
        *    add the first halte to path
        */
        array_unshift($path,$source);

        
        /**
            since the source is not yet insert because it is not yet set (previous[$u] = null) we just add it.
        */

        $last_position = count($path) - 1;
        /**
            GETTING THE CORRECT LINE
        */
        $lines = array();
        
        for($i = 0;$i<count($path);$i++)
        {
            /**
            *   $next = halte berikutnya
            */
            $next = $i + 1;

            foreach($neighbours[$path[$i]] as $a)
            {
                if($next == count($path))
                {
                    break;
                }
                
                if($path[$next] == $a["end"])
                {
                    $lines[$path[$i]][] =  $a["id_line"];
                }
                            
            }
        }


       /**
       *TESTING
       **/
       
       /*
$line = $lines[$path[0]];
       for($x = 0; $x < count($line);$x++)
       {
         $count = Dijkstra::getlineCounter($lines,$line[$x],$path);
         //break;
         echo "The total count for ".$line[$x]." is = ".$count;
         echo "<br/>";
       }
*/
       

       /*
$counter= 1;
       $prevLine = null;
       $tempLines =  $lines;
       for($i = 0 ; $i < count($lines); $i++)
       {
            $line = $tempLines[$path[$i]];
            
            asort($line);
            print_r($line);
            echo "<br/>";
            $currLine = reset($line);
            if($i > 0)
            {
                echo "i = ".$i;
                echo "<br/>";
                echo "current line = ".$currLine;
                echo "<br/>";
                echo "prev line = " .$prevLine;
                echo "<br/>";
                if($prevLine == $currLine)
                {
                    $counter++;
                    echo "counter = ".$counter;
                    echo "<br/>";
                }else
                {
                    break;
                }
            }
            $prevLine = $currLine;
       }
*/
       
       /**
       * END TESTING
       */
/*        exit();        */
       
/*
        $correctLines = array();
        $i = 0;
        $previousLine = null;
        foreach($lines as $line)
        {
            //sort the array
             asort($line);
                   $correctLines[$i] = reset($line);
            $currentLine = reset($line);
            if($currentLine != $previousLine && $previousLine != null)
            {
                foreach($line as $l)
                {
                   
                    if($previousLine == $l)
                    {
                        $correctLines[$i] = $l;
                    }
                }
            }
 
            //get the first position from array
            
            $i++;
            $previousLine = $currentLine;
        }
*/

        Dijkstra::getline($lines,$path,array());
        
        $result[ "path" ] = $path ;
        $result[ "line" ] = Dijkstra::$correctLines;
/*         print_r($result["line"]); */
/* exit(); */
        return $result ;
    }

    
    public function getlineCounter($arrayLines,$trajectLine,$path)
    {
        $counter = 0;
/*         echo "traject line to find =".$trajectLine; */
/*         echo "<br/>"; */
        for($i = 0 ; $i < count($arrayLines); $i++)
        {
            $line = $arrayLines[$path[$i]];
            asort($line);
            
/*             echo "line = "; */
/*             print_r($line); */
/*             echo "<br/>"; */
 
                /**
                *   CHECK IF THE NEXT ARRAY WE HAVE CURRENT TRAJECT LINE;
                **/
                $flag = false;
                if($i+1 < count($arrayLines))
                {
                    $nextLine = $arrayLines[$path[$i+1]];
                    asort($nextLine);
                    for($n = 0;$n < count($nextLine);$n++)
                    {
                        if($nextLine[$n] != $trajectLine)
                        {
                            $flag = true;
                        }else
                        {
                            $flag = false;
                            break;
                        }
                    }
                }
                

                 
                            

                for($p = 0;$p < count($line); $p++)
                {
                    if($line[$p] == $trajectLine)
                    {
                        $counter++;
/*                             echo "counter = ".$counter; */
/*                             echo "<br/>"; */
                            
                    }
                                        
                }
                
                
                
                if($flag)
                    {
/*                         echo "breakking"; */
/*                         echo "<br/>"; */
                        break;
                    }

               
            }
/*        echo "at last = "; */
/*        print_r($counter); */
/*        echo "</br>"; */
/*               echo "</br>"; */
        return $counter;
    }
    
    public function getline($lines,$path,$absLines)
    {

        $absoluteLines = $absLines;
        $tmpLines = $lines;
        $lineCounter = array();        
        $line = $tmpLines[$path[0]];
        
        /*
for($i = 0;$i < count($lines);$i++)
        {
            $line = $lines[$path[$i]];
            asort($line);
            print_r($line);
            echo "<br/>";
        }
        exit();
*/
        for($i = 0; $i < count($line); $i++)
        {
            /**
            *   TRY TO FIND THE furtherest line
            */
            $lineCounter[$line[$i]] = Dijkstra :: getlineCounter($tmpLines,$line[$i],$path);
/*             echo $line[$i]."||"; */

        }


/*         print_r(count($lines)); */
/* print_r($lineCounter); */
/*         echo "<br/>"; */
/*         echo "<br/>"; */
/*         echo count($path); */
/*         print_r($path); */
/*         echo "<br/>"; */

        /**
        *    SORT IT BASE ON DSC VALUE
        */
        arsort($lineCounter);
      //  print_r($tmpLines);
            /**
            *   GET THE FIRST ONE
            *   NOTES: you can make a small calculation to get multiple line if the have the same longest value for example :(id_line=>value) 315=>14,316=>14
            */
/*             echo "line counter = ".reset($lineCounter); */
            for($i = 0;$i < reset($lineCounter);$i++)
            {
/*                 echo "<br/>"; */
/*                 echo $path[$i]; */
                /**
                *   PUSH THE LINE_ID TO THE ARRAY AND REMOVE IT FROM OUR tmpLines array
                */
                array_push($absoluteLines,key($lineCounter));
                unset($tmpLines[$path[$i]]);
            }
            /**
            *   REMOVE FROM path too
            */
            array_splice($path,0,reset($lineCounter));
       
        
        
/*         echo "<br/>"; */
/*         echo "<br/>"; */
/*         echo "<br/>"; */
/*         print_r($tmpLines); */
/*         echo "<br/>"; */
/*         print_r($path); */
        
        /**
        *   CHECK IF tmpLines is not 0 if yes then make it recursive
        */
        if(count($tmpLines)!=0)
        {
            Dijkstra::getline($tmpLines,$path,$absoluteLines);
        }else
        {
/*             echo "<br/>"; */
            
/*             print_r($absoluteLines); */
             Dijkstra::$correctLines = $absoluteLines;
        }
        
        
/*         echo "Key = ".key($lineCounter); */
/*         echo "Value = ".reset($lineCounter); */
    }
    
    public function test()
    {
        echo "test";
    }
}

