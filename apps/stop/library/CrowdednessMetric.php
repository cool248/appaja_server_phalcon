<?php

namespace Appaja\API\Stop\Library ;

class CrowdednessMetric 
{    
    public static function crowdness($crowdednessMetric)
    {
        $arr = null;
        switch($crowdednessMetric)
        {
            case ($crowdednessMetric >= 1 && $crowdednessMetric <= 30) :
                $arr['status'] = 1;
                break;
            
            case ($crowdednessMetric >= 31 && $crowdednessMetric <= 60) :
                $arr['status'] = 2;
                break;
            
            case ($crowdednessMetric >= 61 && $crowdednessMetric <= 100) :
                $arr['status'] = 3;
                break;
            
            case ($crowdednessMetric == 0):
                $arr['status'] = 0;
                break;
            
            default:
                $arr['status'] = 0;
                break;
        }        
        return $arr['status'];
    }
}