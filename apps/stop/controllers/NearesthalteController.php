<?php
//NearestHalteController
namespace Appaja\API\Stop\Controllers ;

use Phalcon\Db\Column, 
    Phalcon\Mvc\Controller,
    Phalcon\Mvc\Model\Query, 
    Phalcon\Mvc\Model\Resultset, 
    Appaja\API\Stop\Library\Dijkstra,
    Appaja\API\Stop\Library\CrowdednessMetric, 
    Appaja\API\Stop\Models\Stps,
    Appaja\API\Stop\Models\StpRprts,
    Appaja\API\Stop\Models\Users, 
    Appaja\API\Stop\Models\StpDtls,
    Appaja\API\Stop\Library\DateUtils, 
    Appaja\API\Transportation\Models\TrjctLns, 
    Appaja\API\Transportation\Models\Trjcts ;

include '../apps/transportation/models/TrjctLns.php' ;
include '../apps/transportation/models/Trjcts.php' ;

class NearesthalteController extends Controller
{
    public function indexAction()
    {
        echo "test";
    }
    public function getAction()
    {
        $latitude = $this->request->getQuery( 'latitude', 'float' ) ;
        $longitude = $this->request->getQuery( 'longitude', 'float' ) ;

        
        if( !empty( $latitude ) && !empty( $longitude ))
        {
            $slct = 'ST_Distance(grtcl,ST_SetSRID(ST_MakePoint('.$longitude.','.
                    $latitude.'),4326)) AS dstnc' ;
                
            do {
                $stp_orgn = Stps::findFirst( array( 
                    'columns' => 'id,'.$slct.',lttd,lngtd', 
                    'order' => 'dstnc'
                ) ) ;
                
                
            } while( $stp_orgn == FALSE ) ;
        }
        
        print_r(json_encode($stp_orgn));
    }
    
    
    public function getNearestHalteForReportingAction()
    {
        $latitude = $this->request->getQuery( 'latitude', 'float' ) ;
        $longitude = $this->request->getQuery( 'longitude', 'float' ) ;
        $distance = $this->request->getQuery( 'distance', 'int' ) ;
        $userDeviceId = $this->request->getQuery('userDeviceId','int');
        $userId = $this->request->getQuery('userId','int');
        $data = array();
        $maksTimeToWait = 15;
        /**
        *MAKE SURE THE USER DEVICE IS NOT EMPTY
        */
        if(empty($userId))
        {
            $data['status'] = false;
            $data['message'] = "user id can't be empty";
            echo json_encode($data);
            exit();
           
        }
        

        /**
        *   MAKE SURE THE USER DOES EXITS
        **/
         $users = Users::find(array(
                "conditions" => "id = ?1",
                "bind" => array(1 => $userId)
            ));
            
            if(empty($users))
            {
                $data['status'] = false;
                $data['message'] = "Users ID not found";
                echo json_encode($data);
                exit();
            }
           
        /**
        *   if the distance is not provided the default will be 200 meter
        */
        if(empty($distance))
        {
            $distance = 200;
        }
         
               
        if(!empty($latitude) && !empty($longitude))
        {

            $sql = "select Distinct (s.id),s.id,sd.nm,s.lttd,s.lngtd,s.typ,
                    ST_Distance(geog,ST_GeographyFromText('POINT($longitude $latitude)')) as distance 
                    from [Appaja\API\Stop\Models\Stps] as s,[Appaja\API\Stop\Models\StpDtls] as sd
                    where s.id = sd.stp and
                    ST_DWithin(ST_GeographyFromText('Point($longitude $latitude)'),geog,$distance)
                    order by distance asc";
           $query = $this->modelsManager->createQuery($sql);
           $stops = $query->execute();
            
            $data ['status'] = false;
            $data ['halte'] = null;
            
            foreach($stops as $s)
            {  
                /**
                *   GET FOR THIS DEVICE ID FOR THIS STOP WITH TIMESTAMP DESC
                */
                $stopReportUser = StpRprts::findFirst(array(
                    "conditions" => 'id_user = ?1 and stp = ?2 and reduced_value != ?3',
                    "bind" => array(1 => $userId, 2 => $s->id, 3 => '0'),
                    "order" => "time_stamp desc"
                ));

                $crowdNess = 0;
                if(!empty($stopReportUser))
                {
                    /**
                    *      MAKE SURE THE CURRENT REPORT TIME IS MORE THAN 15 MINUTES
                    **/
                    $reported = false;
                    $minutes = DateUtils::dateDiffInMinute(null,$stopReportUser->time_stamp);
                     if($minutes > $maksTimeToWait)
                     {
                        $reported = false;
                     }else
                     {
                        $crowdNess = CrowdednessMetric::crowdness($stopReportUser->vl);
                        $reported = true;
                     }
                     
                    $data['halte'][] = array(
                    "distance" => $s->distance,
                    "name" => $s->nm,
                    "latitude" => $s->lttd,
                    "longitude" => $s->lngtd,
                    "type" => $s->typ,
                    "reported" => $reported,
                    "crowdness" => $crowdNess,
                    "id" => $s->id );
                    
                }else
                {
                    $data['halte'][] = array(
                    "distance" => $s->distance,
                    "name" => $s->nm,
                    "latitude" => $s->lttd,
                    "longitude" => $s->lngtd,
                    "type" => $s->typ,
                    "reported" => false,
                    "crowdness" => $crowdNess,
                    "id" => $s->id );
                }
                
                
                    
            }
            
            if(!is_null($data['halte']))
            {
                $data['status'] = true;
            }else
            {
                $data['status'] = false;
            }
        }else
        {
            $data['status'] = false;
            $data ['halte'] = null;
            $data ['message'] = "Latitude and Longitude are every Important,Please provide Latitude and Longitude";
        }   
        
        echo json_encode($data);
    }
}