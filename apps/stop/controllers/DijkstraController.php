<?php

namespace Appaja\API\Stop\Controllers ;

use Phalcon\Db\Column, 
    Phalcon\Mvc\Controller, 
    Phalcon\Mvc\Model\Resultset, 
    Appaja\API\Stop\Library\Dijkstra, 
    Appaja\API\Stop\Library\DepthFirstSearch, 
    Appaja\API\Stop\Models\Stps, 
    Appaja\API\Stop\Models\StpDtls, 
    Appaja\API\Stop\Models\Color,
    Appaja\API\Transportation\Models\TrjctLns, 
    Appaja\API\Transportation\Models\Trjcts ;

include '../apps/transportation/models/TrjctLns.php' ;
include '../apps/transportation/models/Trjcts.php' ;

class DijkstraController extends Controller
{
    
    public function calculateAction()
    {   
        $stp_orgn_id = $this->request->getQuery( 'orgn', 'int' ) ;
        $stp_dstntn_id = $this->request->getQuery( 'dstntn', 'int' ) ;
        $stp_orgn_lttd = $this->request->getQuery( 'orgn_lttd', 'float' ) ;
        $stp_orgn_lngtd = $this->request->getQuery( 'orgn_lngtd', 'float' ) ;
        $stp_dstntn_lttd = $this->request->getQuery( 'dstntn_lttd', 'float' ) ;
        $stp_dstntn_lngtd = $this->request->getQuery( 'dstntn_lngtd', 'float' ) ;
        
       /*
 $stp_orgn_lttd = $this->request->getPost( 'orgn_lttd', 'float' ) ;
        $stp_orgn_lngtd = $this->request->getPost( 'orgn_lngtd', 'float' ) ;
        $stp_dstntn_lttd = $this->request->getPost( 'dstntn_lttd', 'float' ) ;
        $stp_dstntn_lngtd = $this->request->getPost( 'dstntn_lngtd', 'float' ) ;
*/
        
/*         echo($stp_orgn_lttd);exit(); */
        
        if( !empty( $stp_orgn_lttd ) && !empty( $stp_orgn_lngtd ) && 
            !empty( $stp_dstntn_lttd ) && !empty( $stp_dstntn_lngtd ) )
        {
            $slct = 'ST_Distance(grtcl,ST_SetSRID(ST_MakePoint('.$stp_orgn_lngtd.','.
                    $stp_orgn_lttd.'),4326)) AS dstnc' ;
                
            do {
                $stp_orgn = Stps::findFirst( array( 
                    'columns' => 'id,'.$slct, 
                    'order' => 'dstnc'
                ) ) ;
            } while( $stp_orgn == FALSE ) ;
            $stp_orgn_id = $stp_orgn->id ;
/*             print_r($stp_orgn_id); */
            
            $slct = 'ST_Distance(grtcl,ST_SetSRID(ST_MakePoint('.$stp_dstntn_lngtd.','.
                    $stp_dstntn_lttd.'),4326)) AS dstnc' ;
            
            do {
                $stp_dstntn = Stps::findFirst( array( 
                    'columns' => 'id,'.$slct, 
                    'order' => 'dstnc'
                ) ) ;
            } while( $stp_orgn == FALSE ) ;
            $stp_dstntn_id = $stp_dstntn->id ;
/*                         print_r($stp_dstntn_id); */
        }
        
/*         exit(); */
        

        $lns = TrjctLns::find( array( 
            'columns' => 'trjct,stp_frm,stp_t,cst,drtn', 
            'hydration' => Resultset::HYDRATE_ARRAYS ) )->toArray() ;
        
       /*
 echo $stp_orgn_id;
        echo ",";
        echo $stp_dstntn_id;
*/
        //exit();
                
        $dijkstra_clcltd = Dijkstra::clc( $lns, $stp_orgn_id, $stp_dstntn_id ) ;
        

        $pth = $dijkstra_clcltd[ 'path' ];
        $ln =  $dijkstra_clcltd[ 'line' ];

        //$ln = array_unique( $dijkstra_clcltd[ 'line' ] ) ;
        
        $rts = $this->getData($ln,$pth);
        echo json_encode( array( 'errr' => 0, 'dt' => array( 'rts' => $rts ) ) ) ;
    }
    
    
    private function getData($line, $path)
    {
        /**
        *GET ALL THE HALTE NAME
        **/
        $data = array();
        for($i = 0; $i < count($path);$i++)
        {
 
          /**
                GET THE POSITION
            **/
            $haltePosition = Stps::findFirst($path[$i]);
            
            /**
                GET THE DETAILS
            **/
            $halteName = StpDtls::findFirst("stp = '".$path[$i]."'");
            
            /**
                GET THE POLYLINE
            */
            if(count($path) != ($i+1))
            {
                 $conditions = "stp_frm = ?1 AND stp_t = ?2 AND trjct = ?3";
                 $parameters = array(
                    1 => $path[$i],
                    2 => $path[$i+1],
                    3 => $line[$i]);
                    
                $trjctLns = TrjctLns::find(array(
                        $conditions,
                        "bind" =>$parameters))->toArray(); 

            }

            /**
                STORE THE DATA
            **/

            $currentData = null;
            $currentData = array();
            $currentData["id_halte"] = $path[$i];
            $currentData["halte"] = $halteName->nm;
            $currentData["lat"] = $haltePosition->lttd;
            $currentData["long"] = $haltePosition->lngtd;
            
            if($haltePosition->crwdnss == null)
            {
                $currentData["crwd"] = 0;
            }else{
                $currentData["crwd"] = $haltePosition->crwdnss;
            }
            
            
            /**
                FOR THE LAST HALTE NO POLYLINE AND ID_LINE SO NEED THIS CHECKING
            **/    
            if(count($path) != ($i+1))
            {
                
                /**
                    GET THE COLOR
                **/
                $trjcts = Trjcts::findFirst($trjctLns[0]['trjct']);
                $currentData["id_line"] = $line[$i];
                if($trjcts->nm == null)
                {
                    $currentData["line"] = "";
                }else
                {
                    $currentData["line"] = $trjcts->nm;
                }
                $currentData["polyline"] = $trjctLns[0]['plyln'];
                $currentData["duration"] = $trjctLns[0]['drtn'];
                $currentData["cost"] = $trjctLns[0]['cst'];
                if($trjcts->typ == null)
                {
                    $currentData["id_description"] = 0;    
                }else
                {
                    $currentData["id_description"] = $trjcts->typ;
                }
                
                
            }   
            
            if($trjcts->clr == null)
                {
                    $currentData["clr"] = "#777777";
                }else
                {
                    $currentData["clr"] = $trjcts->clr;
/*                     print_r("test");exit(); */
                    $colorFront = Color::findFirst($trjcts->clr_front);
                    $colorBack = Color::findFirst($trjcts->clr_back);
                    $currentData["clr_front"] = $colorFront->color;
                    $currentData["clr_back"] = $colorBack->color;
                }
            
            $data[$i] = array($currentData);
            
                    
                    
            /*
print_r($haltePosition->lttd);
            echo ",";
            print_r($haltePosition->lngtd);
            echo ",";
            print_r($halteName->nm);
            echo ",";
            print_r($haltePolyline[0]['plyln']);
            echo "<br/>";
*/
        }
/*         print_r(json_encode($data)); */
/*         print_r($data); */
        return $data;
    }


    /**
     * Find line
     * @param type $line
     * @param type $path
     * @return type
     */
    private function ln_fnd( $line, $path ) 
    {
        $maks_segment = count( $path ) - 1 ;

        for( $i = 0; $i < count( $path ); $i++ ) 
        {
            $next = $i + 1 ;

            if( $i == $maks_segment ) 
            {
                $id_halte = $path[ $maks_segment ] ;
                
                $stp = Stps::findFirst( array( 
                    'conditions' => 'id = '.$id_halte, 
                    'hydration' => Resultset::HYDRATE_ARRAYS ) )->toArray() ;
                $stp[ 'nm' ] = StpDtls::findFirst( array( 
                    'conditions' => 'stp = '.$stp[ 'id' ].' AND lngg = 1'
                ) )->nm ;
                
                $route_list[] = $stp ;
            }

            if ($next == count($path)) 
            {
                break;
            }

            $id_halte = $path[$i];
            $next_halte = $path[$next];


            foreach( $line as $l ) 
            {
                $cndtns = 'stp_frm = :stp_frm: AND stp_t = :stp_t: AND trjct = :trjct:' ;
                $lns = TrjctLns::find( array( 
                    'conditions' => $cndtns, 
                    'bind' => array( 'stp_frm' => $id_halte, 'stp_t' => $next_halte, 
                        'trjct' => $l ), 
                    'bindType' => array( 'stp_frm' => Column::BIND_PARAM_INT, 
                        'stp_t' => Column::BIND_PARAM_INT, 
                        'trjct' => Column::BIND_PARAM_INT ), 
                    'hydration' => Resultset::HYDRATE_ARRAYS ) )->toArray() ;
                
                $cnt = count( $lns ) ;
                for($ii=0;$ii<$cnt;$ii++)
                {
                    $ln = &$lns[ $ii ] ;
                    $ln[ 'nm' ] = Trjcts::findFirst( array( 
                        'conditions' => 'id = '.$ln[ 'trjct' ]
                    ) )->nm ;
                }
                
                if( count( $lns ) != 0 )
                {
                    $route_list[] = $lns ;
                    break;
                }
            }
        }
        return $route_list;
    }
    
    
    
    public function searchAction()
        {
        
    
         ini_set('max_execution_time', 0);
         ini_set('memory_limit', '4096M');
         
     $incidenceList = array(
        1 => array(
            'vertex' => 1, // vertex number
            'visited' => false, // `visited` flag
            'letter' => 'a', // vertex value
            'neighbours' => array(2,3,5), // neighbours
            'previous'=> ''
        ),
        2 => array(
            'vertex' => 2,
            'visited' => false,
            'letter' => 'd',
            'neighbours' => array(1,4,3),
            'previous'=> ''
        ),
        3 => array(
            'vertex' => 3,
            'visited'=> false,
            'letter' => 'b',
            'neighbours' => array(1,4,5,2),
            'previous'=>   ''              
        ),
        4=> array(
            'vertex' => 4,
            'visited'=> false,
            'letter' => 'c',
            'neighbours' => array(2,3),
            'previous'=> ''
        ),
        5 => array(
            'vertex' => 5,
            'visited'=> false,
            'letter' => 'e',
            'neighbours' => array(1,3),
            'previous'=> ''
        )
    );
    
    
    $_search = new DepthFirstSearch();
   
    
    $lns = TrjctLns::find( array( 
                'columns' => 'trjct,stp_frm,stp_t,cst,drtn', 
                'hydration' => Resultset::HYDRATE_ARRAYS ) )->toArray() ;
   
    $_graph = $_search->getGraph($lns);
    
    /* print_r(json_encode($_graph));exit(); */
    /* print_r(json_encode($incidenceList));exit(); */
    $start = 75;
    $end = 33;
    $startList = null;
    
    /* self::$track[self::$index][] = $start; */
    
    /*Get the start list*/
    foreach($_graph as $a)
    {
        if($a['letter'] == $start)
        {
            $startList = $a;
            break;
        }
    }
    
    /* echo "The start is = "; */
    /* print_r($startList);exit(); */
    
    
    $_search->search($start,$end,$startList,$_graph);
    $_search->printTrack();
    exit();
    
    //$value =$this->depthFirstSearch($start,$end,$startList,$_graph);
    
    /* echo "The value is = ".$value; */
    //$this->printTrack();
    
    }      
    
}
