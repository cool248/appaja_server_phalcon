<?php

namespace Appaja\API\Stop\Controllers ;

use Phalcon\Db\Column, 
    Phalcon\Mvc\Controller,
    Appaja\API\Stop\Models\StpRprts, 
    Appaja\API\Stop\Models\Stps,
    Appaja\API\Stop\Models\Users,
    Appaja\API\Stop\Library\CrowdednessMetric,
    Appaja\API\Stop\Library\DateUtils,
    Appaja\API\Stop\Models\CronJob ;

class StopController extends Controller
{
    
    /**
     * Report 
     */
    public function reportAction()
    {
        $stp_id = $this->request->getQuery( 'stp', 'int' ) ;
        $vl = $this->request->getQuery( 'vl', 'int' ) ;
        
        if( !empty( $stp_id ) && !empty( $vl ) )
        {
            $rprt = new StpRprts() ;
            $rprt->stp = $stp_id ;
            $rprt->dt = date( 'Y-m-d' ) ;
            $rprt->vl = $vl ;
            $rprt->save() ;
            
            $crtr = array( 
                'conditions' => 'stp = :stp_id: AND dt = :dt:', 
                'bind' => array( 'stp_id' => $stp_id, 
                    'dt' => date( 'Y-m-d' ) ), 
                'bindTypes' => array( 'stp_id' => Column::BIND_PARAM_INT )
            ) ;
            $cnt = StpRprts::count( $crtr ) ;
            $sm = StpRprts::sum( array( 'column' => 'vl' ) + $crtr ) ;
            $crwdnss = round( floatval( $sm / $cnt ), 2 ) ;
            
            $stp = Stps::findFirst( $stp_id ) ;
            $stp->crwdnss = $crwdnss ;
            $stp->save() ;
        }
    }
    
    /**
    *   Report system
    *   @params 
    *   stopID(int)
    *   users_device_id
    *   report value
    *   
    **/
    public function reportHalteAction()
    { 
        /**
        *   MAKS TIME TO WAIT
        */
        $maksTimeToWait = 15;
        
        $stopId = $this->request->getQuery( 'stopId', 'int' ) ;
        $reportValue = $this->request->getQuery( 'reportValue', 'int' ) ;
        $idUser = $this->request->getQuery( 'idUser', 'int' ) ;
        $data = array();
        $data['status'] = true;
        $data['message'] = "";
        
        
        
        if(empty($stopId))
        {
            $data['status'] = false;
            $data['message'] = $data['message']."stop id is missing ||";
        }else
        {
            $stops = Stps::findFirst(array(
                "conditions" => "id = ?1",
                "bind" => array(1 => $stopId)
            ));
            
            if(empty($stops))
            {
                $data['status'] = false;
                $data['message'] = $data['message']."stop ID not found ||";
            }
        }
        
        if(empty($reportValue))
        {
            $data['status'] = false;
            $data['message'] = $data['message']." report value is missing ||";
        }else
        {
            if($reportValue > 3 || $reportValue < 1)
            {
                $data['status'] = false;
                $data['message'] = $data['message']." report value is should be between 1 to 3 ||";
            }
        }
        
        if(empty($idUser))
        {
            $data['status'] = false;
            $data['message'] = $data['message']." idUser value is missing";
        }else
        {
            $users = Users::findFirst(array(
                "conditions" => "id = ?1",
                "bind" => array(1 => $idUser)
            ));
            
            if(empty($users))
            {
                $data['status'] = false;
                $data['message'] = $data['message']." Users ID not found";
            }
        }
        
        if($data['status'] == false)
        {
            echo json_encode($data);
            exit();
        }
        
        /**
        *   check if this user has already reported this halte and maks time 15 menits
        **/
        $time = StpRprts::findFirst(array(
            "columns" => "time_stamp",
            "conditions" => "id_user = ?1 and stp = ?2",
            "bind" => array(1=>$idUser,2=>$stopId),
            "order" => "time_stamp desc"
        ));


        /**
        *   MAKE SURE THE $time is not empty
        */
        if(!empty($time))
        {
            $minutes = DateUtils::dateDiffInMinute(null,$time->time_stamp);
            if($minutes < $maksTimeToWait)
            {
                $data['status'] = false;
                $data['message'] = "Already Reported";
                echo json_encode($data);
                exit();
            }
        }    
        
        /**
        *   convert the report value to system value
        *   1 = 30
        *   2 = 60
        *   3 = 100
        **/
        switch($reportValue)
        {
            case 1:
                $reportValue = 30;
                break;
            case 2:
                $reportValue = 60;
                break;
            case 3:
                $reportValue = 100;
                break;
            default:
                break;
        }
        
        /**
        *   Save the data for log
        */
        
        $_objct = new StpRprts();
        $_objct->stp = $stopId;
        $_objct->vl = $reportValue;
        $_objct->reduced_value = $reportValue;
        $_objct->id_user = $idUser;
        $_objct->create();
        
        /**
        *   accumulate/calculate the total stpRprts for the current halte and get crowded metrics and set in table stops
        **/
         $result = StpRprts::find(array(
            "conditions" => 'reduced_value != ?1 and stp = ?2',
            "bind" => array(1 => '0',2 =>$stopId),
            "order" => "id asc"
        ));
        
        $accumulatedStopValue = array();
        foreach ($result as $r) 
        {            
             $accumulatedStopValue[$stopId][] =  $r->reduced_value;
        }
        
        $addMetrics = 0;
        /**
        *    STOPS
        */
        foreach($accumulatedStopValue as $k => $value)
        {
           $addMetrics = 0;
           /**
           *    calculate all the value
           */
           foreach($value as $v)
           {
               $addMetrics = $addMetrics + $v;
           }
           
           /***
           *    find the crowdness metrics
           **/
           $endValueMetric = CrowdednessMetric::crowdness(round($addMetrics / count($value)));
           
           $currentStop = Stps::findFirst(array(
                "conditions" => 'id = ?1',
                "bind" => array(1 => $k)
           ));
           
           $currentStop->crwdnss = $endValueMetric;
           $currentStop->update();
        }
        
                
        $data['status'] = true;
        $data['message'] = "Thank you for reporting";
        echo json_encode($data);
        
    }
    
    
    
    public function testAction()
    {
        date_default_timezone_set("Asia/Bangkok");
        $start_date = new \DateTime(date('Y-m-d H:i:s'));
        print_r($start_date);
        exit();
        
        $result = CronJob::find();
        $cronJob = new CronJob();
        $text = "test".(count($result)+1);
        $cronJob->some_text = $text;
        $cronJob->save();

        $result = CronJob::find();
        echo "There are ", count($result), "\n";
        
    }
    
    
    
}