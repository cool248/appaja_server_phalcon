<?php

namespace Appaja\API\Stop\Controllers ;

use Phalcon\Db\Column, 
    Phalcon\Mvc\Controller, 
    Phalcon\Mvc\Model\Resultset, 
    Appaja\API\Stop\Library\Dijkstra,
    Appaja\API\Stop\Library\DepthFirstSearch, 
    Appaja\API\Stop\Models\Stps, 
    Appaja\API\Stop\Models\StpDtls, 
    Appaja\API\Stop\Models\Color,
    Appaja\API\Transportation\Models\TrjctLns, 
    Appaja\API\Transportation\Models\Trjcts ;

include '../apps/transportation/models/TrjctLns.php' ;
include '../apps/transportation/models/Trjcts.php' ;

class IndexController extends Controller
{
    public function indexAction()
    {
	header('Content-Type: text/plain');
	echo "Server IP: ".$_SERVER['SERVER_ADDR'];
	echo "\nX-Forwarded-for: ".$_SERVER['HTTP_X_FORWARDED_FOR'];

    }
}
