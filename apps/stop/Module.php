<?php

namespace Appaja\API\Stop ;

use Phalcon\Loader,
    Phalcon\Mvc\Dispatcher, 
    Phalcon\Mvc\ModuleDefinitionInterface, 
    Phalcon\Mvc\View,
    Phalcon\Mvc\Model\Manager ;

class Module implements ModuleDefinitionInterface
{
    
    /**
     * Register a specific autoloader for the module
     */
    public function registerAutoloaders()
    {
        $loader = new Loader() ;
        $loader->registerNamespaces( array(
            'Appaja\API\Language\Models' => '../apps/language/models', 
            'Appaja\API\Stop\Controllers' => '../apps/stop/controllers/', 
            'Appaja\API\Stop\Library' => '../apps/stop/library/', 
            'Appaja\API\Stop\Models' => '../apps/stop/models/'
        ) )->register() ;
    }
    
    /**
     * Register services
     */
    public function registerServices( $di )
    {
        $di->set( 'dispatcher', function() {
            $dispatcher = new Dispatcher() ;
            $dispatcher->setDefaultNamespace( 'Appaja\API\Stop\Controllers' ) ;
            
            return $dispatcher ;
        } ) ;
        
        $di->set( 'view', function() {
            $view = new View() ;
            $view->setViewsDir( '../apps/stop/views' ) ;
            
            return $view ;
        } ) ;
        
        $di->set('modelsManager', function() {
      return new Manager();
 });
    }
    
}

