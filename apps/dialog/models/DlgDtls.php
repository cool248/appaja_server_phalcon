<?php

namespace Appaja\API\Dialog\Models ;

use Phalcon\Mvc\Model, 
    Appaja\API\Dialog\Models\Dlgs, 
    Appaja\API\Language\Models\Lnggs ;

class DlgDtls extends Model
{
    
    /**
     * The ID
     * @access protected
     * @var int 
     */
    protected $id ;
    
    /**
     * The dialog
     * @access public
     * @var int 
     */
    public $dlg ;
    
    /**
     * The language
     * @access public
     * @var int 
     */
    public $lngg ;
    
    /**
     * The text
     * @access protected
     * @var type 
     */
    private $txt ;
    
    public function getId()
    {
        return $this->id ;
    }
    
    public function getTxt()
    {
        return $this->txt ;
    }
    
    /**
     * Initialize
     */
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships
        $this->belongsTo( 'dlg', 'Dlgs', 'id' ) ;
        $this->belongsTo( 'lngg', 'Lnggs', 'id' ) ;
    }
    
}
