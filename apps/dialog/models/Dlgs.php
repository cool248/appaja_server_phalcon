<?php

namespace Appaja\API\Dialog\Models ;

use Phalcon\Mvc\Model, 
    Appaja\API\Dialog\Models\DlgDtls ;

class Dlgs extends \Phalcon\Mvc\Model
{
    /**
     * The ID
     * @access public
     * @var int 
     */
    public $id ;
    
    /**
     * The active flag
     * @access public
     * @var boolean 
     */
    public $actv ;
    
    /**
     * Initialize
     */
    public function initialize()
    {
        // Set connection service 
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships 
        $this->hasMany( 'id', 'DlgDtls', 'dlg' ) ;
    }
}
