<?php

namespace Appaja\API\Dialog ;

use Phalcon\Loader, 
    Phalcon\Mvc\Dispatcher, 
    Phalcon\Mvc\ModuleDefinitionInterface, 
    Phalcon\Mvc\View ;

class Module implements ModuleDefinitionInterface 
{
    
    /**
     * Register a specific autoloader for the module
     */
    public function registerAutoloaders()
    {
        $loader = new Loader() ;
        $loader->registerNamespaces( array( 
            'Appaja\API\Dialog\Controllers' => '../apps/dialog/controllers/', 
            'Appaja\API\Dialog\Models' => '../apps/dialog/models/', 
            'Appaja\API\Language\Models' => '../apps/language/models/' )
        )->register() ;
    }
    
    public function registerServices( $di )
    {
        // Set a dispatcher 
        $di->set( 'dispatcher', function() {
            $dispatcher = new Dispatcher() ;
            $dispatcher->setDefaultNamespace( 'Appaja\API\Dialog\Controllers' ) ;
            
            return $dispatcher ;
        } ) ;
        
        // Set view 
        $di->set( 'view', function() {
            $view = new View() ;
            $view->setViewsDir( '../apps/dialog/views' ) ;
            
            return $view ;
        } ) ;
    }
    
}
