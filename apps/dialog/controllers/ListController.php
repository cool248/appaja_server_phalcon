<?php

namespace Appaja\API\Dialog\Controllers ;

use Phalcon\Mvc\Controller, 
    Appaja\API\Dialog\Models\Dlgs, 
    Appaja\API\Dialog\Models\DlgDtls ;

class ListController extends Controller
{
    
    /**
     * Index
     */
    public function indexAction()
    {
        $dlgs = array() ;
        
        $this->dlgs_st( $dlgs ) ;
        
        echo json_encode( array( 'errr' => 0, 
            'dt' => array( 'dlgs' => $dlgs ) ) ) ;
    }
    
    private function clearCacheAction()
    {
        $di->modelsCache->delete("dlgs-lst-indx");
    }
    
    private function dlgs_st( &$dlgs )
    {
        $dlgs_rslt_st = Dlgs::find( array( 
            'cache' => array( 'key' => 'dlgs-lst-indx' ),
            'order' => 'sqnc' ) ) ;
        
        $ii = 0 ;
        while( $dlgs_rslt_st->valid() )
        {
            $rcrd = $dlgs_rslt_st->current() ;
            $dlgs[ $ii ][ 'id' ] = $rcrd->id ;
            
            $dtls_rslt_st = DlgDtls::find( array( 
                'cache' => array( 'key' => 'dlg'.$rcrd->id.'_dtls-lst-indx' ),
                'dlg = '.$rcrd->id ) ) ;
            $jj = 0 ;
            while( $dtls_rslt_st->valid() )
            {
                $dtl_rcrd = $dtls_rslt_st->current() ;
                
                if( $jj == 0 ) 
                {
                    $dlgs[ $ii ][ 'en-us' ] = $dtl_rcrd->getTxt() ;
                }
                else
                {
                    $dlgs[ $ii ][ 'id-id' ] = $dtl_rcrd->getTxt() ;
                }
                
                $jj++ ;
                $dtls_rslt_st->next() ;
            }
            
            $ii++ ;
            $dlgs_rslt_st->next() ;
        }
    }
    
}

