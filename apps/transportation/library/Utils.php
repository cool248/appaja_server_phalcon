<?php
namespace Appaja\API\Transportation\Library ;

use Phalcon\Http\Request,
    Phalcon\DI\FactoryDefault;

class Utils {
    
    public function checkQuery($var,$type)
        {
            $di = new FactoryDefault();
            $request = new Request();
            $request->setDI($di);
            
            $data = array();
            $data['status'] = false;
            $value = null;
            
            if($request->hasQuery($var))
            {
                $value = $request->getQuery($var,$type);
                if(empty($value))
                {
                    $data['message'] = $var." is empty";
                }else
                {
                    $data['status'] = true;
                    $data['message'] = "Good";
                    $data['value'] = $value;
                }
                
            }else
            {
                $data['message'] = $var." Not Avalaible.";
            }
            
            
            if(!$data['status'])
            {
                echo json_encode($data);
                exit();
            }
            return $data;
        }
}