<?php

namespace Appaja\API\Transportation\Controllers ;

use Phalcon\Mvc\Controller,
    Appaja\API\Transportation\Models\TrnsprttnTyps, 
    Appaja\API\Transportation\Models\TrnsprttnTypDtls ;

class TypesController extends Controller
{
    
    /**
     * Action index 
     */
    public function indexAction()
    {
        $typs = array() ;
        
        $this->typs_st( $typs ) ;
        
        echo json_encode( $typs ) ;
    }
    
    /**
     * Set types
     * @param array The categories
     */
    private function typs_st( &$typs )
    {
        $typs_rslt_st = TrnsprttnTyps::find() ;
        
        $ii = 0 ;
        while( $typs_rslt_st->valid() )
        {
            $rcrd = $typs_rslt_st->current() ;
            $typs[ $ii ][ 'id' ] = $rcrd->id ;
            
            $dtls_rslt_st = TrnsprttnTypDtls::find( array( 
                'typ = '.$rcrd->id.' AND lngg = '.$_GET[ 'lngg' ] ) ) ;
            
            while( $dtls_rslt_st->valid() )
            {
                $dtl_rcrd = $dtls_rslt_st->current() ;
                $typs[ $ii ][ 'nm' ] = $dtl_rcrd->nm ;
                
                $dtls_rslt_st->next() ;
            }
            
            $ii++ ;
            $typs_rslt_st->next() ;
        }
    }
    
}
