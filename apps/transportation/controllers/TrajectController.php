<?php

namespace Appaja\API\Transportation\Controllers ;

use Phalcon\Db\Column, 
    Phalcon\DI,
    Phalcon\Db\Adapter\Pdo\Postgresql,
    Phalcon\Mvc\Controller, 
    Phalcon\Mvc\Model\Resultset, 
    Appaja\API\Location\Models\ArDtls, 
    Appaja\API\Location\Models\SbdstrctDtls, 
    Appaja\API\Location\Models\VllgDtls, 
    Appaja\API\Transportation\Models\TrjctRts, 
    Appaja\API\Transportation\Models\TrjctRtsArs, 
    Appaja\API\Transportation\Models\TrjctRtsSbdstrcts,
    Appaja\API\Transportation\Models\TrjctRtsVllgs,
    Appaja\API\Transportation\Models\Trjcts, 
    Appaja\API\Transportation\Models\TrnsprttnCmpnyDtls, 
    Appaja\API\Transportation\Models\TrnsprttnCmpnySrvcs,
    Appaja\API\Transportation\Models\TrjctGngCrdnts,
    Appaja\API\Transportation\Models\TrjctCmngCrdnts,
    Appaja\API\Transportation\Models\Price,
    Appaja\API\Transportation\Models\Ratings,
    Appaja\API\Transportation\Models\TrjctLns,
    Appaja\API\Transportation\Library\Polyline,
    Appaja\API\Transportation\Library\Utils  ;

class TrajectController extends Controller
{
    
    public function lineimportAction()
    {
//        include './db/tbl_connection.php' ;
//        include '../apps/stop/models/Stps.php' ;
//        
//        foreach( $tbl_connection as $connection_bak )
//        {
//            $trjct = intval( $connection_bak[ 'id_line' ] ) ;
//            $stp_frm = Stps::findFirst( 'id_prvs = '.$connection_bak[ 'id_halte' ] )->id ;
//            $stp_t = Stps::findFirst( 'id_prvs = '.$connection_bak[ 'next_halte' ] )->id ;
//            $cst = intval( $connection_bak[ 'cost' ] ) ;
//            $drtn = intval( $connection_bak[ 'duration' ] ) ;
//            $plyln = $connection_bak[ 'polyline' ] ;
//            
//            $ln = new TrjctLns() ;
//            $ln->trjct = $trjct ;
//            $ln->stp_frm = $stp_frm ;
//            $ln->stp_t = $stp_t ;
//            $ln->cst = $cst ;
//            $ln->drtn = $drtn ;
//            $ln->plyln = $plyln ;
//            $ln->save()         ;
//        }
    }
    
    public function getUserReportedTrajectAction()
    {
        $data = array();
        $data['status'] = false;
        $message = "";
        $utils = new Utils();
        $trajectId = $utils->checkQuery('traject_id',"int");
        $userId = $utils->checkQuery('user_id',"int");
        
        $rate = Ratings::findFirst(array(
            'conditions' => "traject_id=?1 AND user_id=?2",
            'bind' => array(1 => $trajectId['value'],2 => $userId['value'])
        ));
        
        if(empty($rate))
        {
            $data['message'] = "never rated this traject";
        }else
        {
            $data['status'] = true;
            $data['data'] = $rate->toArray();
        }
        
        echo json_encode($data);

    }
    
    public function sendRatingAction()
    {
        $data = array();
        $data['status'] = false;
        
        $utils = new Utils();
        $trajectId = $utils->checkQuery('traject_id',"int");
        $userId = $utils->checkQuery('user_id',"int");
        $rating = $utils->checkQuery('rating',null);
        
        if($rating['value'] > 5 || $rating['value'] < 0.5)
        {
            $data['message'] = "The rating must be between 0.5 - 5";
            echo json_encode($data);
            exit();
        }
        
        /**
        *   check if this user has already rated this traject
        */
        $rate = Ratings::findFirst(array(
            'conditions' => "traject_id=?1 AND user_id=?2",
            'bind' => array(1 => $trajectId['value'],2 => $userId['value'])
        ));
        
        if(empty($rate))
        {
           $rate = new Ratings();
           $rate->traject_id =  $trajectId['value'];
           $rate->user_id = $userId['value'];
           $rate->rating = $rating['value'];
           $data['status'] = $rate->save();
        }else
        {
            $date = date('Y-m-d H:i:s', time());
            $rate->rating = $rating['value'];
            $rate->updated_at = $date;
            $data['status'] = $rate->update();
        }
        
        /**
        *   calculate the total value of current trject_id and update to traject_table[rating]
        */

       $average = Ratings::average(array(
            'conditions' =>"traject_id = ?1",
            'bind'=> array(1=>$trajectId['value']),
            'column' => 'rating'
       )); 
        
        $traject = Trjcts::findFirst(array(
            'conditions' => "id = ?1",
            'bind' => array(1 => $trajectId['value'])
        ));
        $traject->rating = doubleval($average);
        $traject->update();
        
        $data['rating_average'] = doubleval($average);
        
        echo json_encode($data);
        
    }
    
    public function testingNewTrajectAction()
    {
   
        $sql = "select * from [Appaja\API\Transportation\Models\Trjcts]";
        $query = $this->modelsManager->createQuery($sql);
        $traject = $query->execute();
        
        $phql = "INSERT INTO [Appaja\API\Transportation\Models\Trjcts] (typ,cmpny,cd,nm,chck_pnts,nmbr,ac,frequency,schedule) values(4,10,'t','tsat','t','no',0,'t','t')";
        $query = $this->modelsManager->createQuery($phql);
        $traject = $query->execute();
        print_r($traject);
    }
    
    
    public function routesAction()
    {
        $trjct = $this->request->getQuery( 'trjct', 'int' ) ;
        $lngg = $this->request->getQuery( 'lngg', 'int', 1 ) ;
        
        $rts = TrjctRts::find( array( 
            'conditions' => 'trjct = :trjct:', 
            'columns' => 'id,typ', 
            'bind' => array( 'trjct' => $trjct ), 
            'bindTypes' => array( 'trjct' => Column::BIND_PARAM_INT ), 
            'hydration' => Resultset::HYDRATE_ARRAYS, 
            'order' => 'sqnc' ) )->toArray() ;
        
        for( $ii = 0 ; $ii < count( $rts ) ; $ii++ )
        {
            $rt = &$rts[ $ii ] ;
            switch( $rt[ 'typ' ] )
            {
                case 1 :
                    $rtVllg = TrjctRtsVllgs::findFirst( array( 
                        'conditions' => 'rt = :rt:', 
                        'columns' => 'vllg', 
                        'bind' => array( 'rt' => $rt[ 'id' ] )
                    ) ) ;
                    $rt[ 'nm' ] = VllgDtls::findFirst( array( 
                        'conditions' => 'vllg = :vllg:, lngg = :lngg:', 
                        'columns' => 'nm', 
                        'bind' => array( 'vllg' => $rtVllg->vllg, 
                            'lngg' => $lngg )
                    ) )->nm ;
                    
                    break ;
                    
                case 2 :
                    $rtSbdstrct = TrjctRtsSbdstrcts::findFirst( array( 
                        'conditions' => 'rt = :rt:', 
                        'columns' => 'sbdstrct', 
                        'bind' => array( 'rt' => $rt[ 'id' ] )
                    ) ) ;
                    $rt[ 'nm' ] = SbdstrctDtls::findFirst( array( 
                        'conditions' => 'sbdstrct = :sbdstrct:, lngg = :lngg:', 
                        'columns' => 'nm', 
                        'bind' => array( 'sbdstrct' => $rtSbdstrct->sbdstrct, 
                            'lngg' => $lngg )
                    ) )->nm ;
                    
                    break ;
                    
                case 3 :
                    $rtAr = TrjctRtsArs::findFirst( array( 
                        'conditions' => 'rt = :rt:', 
                        'columns' => 'ar', 
                        'bind' => array( 'rt' => $rt[ 'id' ] )
                    ) ) ;
                    $rt[ 'nm' ] = ArDtls::findFirst( array( 
                        'conditions' => 'ar = :ar:, lngg = :lngg:', 
                        'columns' => 'nm', 
                        'bind' => array( 'ar' => $rtAr->ar, 
                            'lngg' => $lngg )
                    ) )->nm ;
                    
                    break ;
            }
        }
    }
    
    /**
     * Action index
     */
    public function indexAction()
    {
        $lngg = $this->request->getQuery( 'lngg', 'int', 1 ) ;
        $typ = $this->request->getQuery( 'typ', 'int' ) ;
        //$crdnt = boolval( $this->request->getQuery( 'crdnt', 'int' ) ) ;
        
        $srvcs = TrnsprttnCmpnySrvcs::find( array( 
            'cache' => array( 'key' => 'TrnsprttnCmpnySrvcs_'.$typ ),
            'conditions' => 'typ = ?1 ', 
            'columns' => 'cmpny', 
            'bind' => array( 1 => $typ ), 
            'bindTypes' => array( 1 => Column::BIND_PARAM_INT ) ) ) ;
        

        
        $cmpns = array() ;
        $ii = 0 ;
        foreach( $srvcs as $srvc )
        {
            $cmpns[ $ii ][ 'id' ] = $srvc->cmpny ;
            $dtl =  TrnsprttnCmpnyDtls::findFirst(array(
                        'cache' => array('key' => 'dtl_'.$typ.'_'.$srvc->cmpny),
                        'conditions' => "cmpny = ?1 AND lngg = ?2",
                        'bind'=> array(1 => $srvc->cmpny,2 => $lngg)
                    ));
            //print_r($dtl->nm);
/*
$dtl = TrnsprttnCmpnyDtls::findFirst( 'cmpny = '.$srvc->cmpny. 
                    ' AND lngg = '.$lngg ) ;      
*/                
            
            $cmpns[ $ii ][ 'nm' ] = $dtl->nm ;
            
            $cndtns = 'typ = :typ: AND cmpny = :cmpny:' ;
            
            $cmpns[ $ii ][ 'trjcts' ] = Trjcts::find( array(
                'cache' => array( 'key' => 'trjcts'.$typ.'_'.$ii ),
                'conditions' => $cndtns, 
                'columns' => 'id,cd,nmbr,nm,gng_crdnts_exstd,cmng_crdnts_exstd,chck_pnts,schedule_start,schedule_end,frequency,rating', 
                'bind' => array( 'typ' => $typ, 'cmpny' => $srvc->cmpny ), 
                'bindTypes' => array( 'typ' => Column::BIND_PARAM_INT, 
                    'cmpny' => Column::BIND_PARAM_INT ), 
                'hydration' => Resultset::HYDRATE_ARRAYS ) )->toArray() ;

            for($i = 0; $i < count($cmpns[$ii]['trjcts']);$i++)
            {
                $trjctId = $cmpns[$ii]['trjcts'][$i]['id'];
                $price = Price::findFirst(array(
                    'cache' => array( 'key' => 'price'.'_'.$trjctId ),
                    'conditions' => "id_traject = ?1",
                    'bind' => array(1 => $trjctId)
                ));
                
                $cmpns[$ii]['trjcts'][$i]["fare_near"] = $price->fare_near;
                $cmpns[$ii]['trjcts'][$i]["fare_far"] = $price->fare_far;
            }
                
                //echo "</br>";
            $ii++ ;
        }
        
        echo json_encode( array( 
                'errr' => 0, 
                'dt' => array( 'cmpns' => $cmpns ) 
            ) ) ;
    }
    
//    public function trainImportAction()
//    {
//        include './db/line_krl.php' ;
//        
//        $trjcts = simplexml_load_file( './db/line_krl.xml' ) ;
//        foreach( $trjcts->line as $trjct )
//        {
//            for( $ii = 0 ; $ii < count( $trjct->cp ) - 1 ; $ii++ )
//            {
//                $ln = new TrjctLns() ;
//                $ln->trjct = intval( ( string )$trjct[ 'trjct' ] ) ;
//                $ln->stp_frm = intval( $trjct->cp[ $ii ][ 'id' ] ) ;
//                $ln->stp_t = intval( $trjct->cp[ $ii + 1 ][ 'id' ] ) ;
//                $ln->plyln = strval( $trjct->cp[ $ii ][ 'polyline' ] ) ;
//                $ln->save() ;
//            }
//        }
//    }

    public function getTrajectPolylineByIDAction()
    {
        $data = array();
        /**
        *   GET THE TRAJECTID
        */
         $trajectID = $this->request->getQuery( 'trajectID', 'int' ) ;
         $data["trajectID"] = $trajectID;
         
         if($trajectID == "")
         {
            echo "what is the traject id?Dumb ASS!!";
            exit();
         }
        
        /*
        *   GET THE CURRENT TRAJECT ID DATA
        */
         $trajects = Trjcts::find(array(
            "conditions" => "id = ?1",
            "bind" => array(1 => $trajectID),
            "columns" => "typ,polyline_going,polyline_coming"
         ));
         
         
         if($trajects->count() == 0)
         {
                $dataGoing["status"] = false;
                $dataGoing["polyline"] = "";
                $dataGoing["message"] = "Data not available";
                
                $dataComing["status"] = false;                
                $dataComing["polyline"] = "";
                $dataComing["message"] = "Data not available";
                
                $data["going"] = $dataGoing;
                $data["coming"] = $dataComing;
                
                echo json_encode($data);
                exit();
         }
         
         /**
         *  CHECK THE TYPE IF NOT 3 OR 5 THEN CONTINUE
         */
         
        /*
 if($trajects->getFirst()->typ == 3 || $trajects->getFirst()->typ == 5)
         {            $postgreSQL = $this->di->get("PostgreSQL");
            $sql = "select distinct cst,plyln from trjct_lns where trjct =".$trajectID;
            $trajectOfficial = $postgreSQL->query($sql);
            
            if($trajectOfficial->numRows() == 0)
            {
                $dataGoing["status"] = false;
                $dataGoing["polyline"] = "";
                $dataGoing["message"] = "Data not available";
                
                $dataComing["status"] = false;                
                $dataComing["polyline"] = "";
                $dataComing["message"] = "Data not available";
                
                $data["going"] = $dataGoing;
                $data["coming"] = $dataComing;

            }else
            {
                $polyline = "";
                foreach($trajectOfficial->fetchAll() as $tf)
                {   
                    print_r($tf['plyln']);
                    echo "</br>";
                    echo "</br>";
                    $polyline = $tf['plyln'];
                }
                
                $dataGoing["status"] = true;
                $dataGoing["polyline"] = $polyline;
                $dataGoing["message"] = "";
                
                $dataComing["status"] = true;                
                $dataComing["polyline"] = $polyline;
                $dataComing["message"] = "";
                
                $data["going"] = $dataGoing;
                $data["coming"] = $dataComing;
                
            }
            
            
            echo json_encode($dataGoing["polyline"]);
            exit();
            
            
            
         }
         
*/
         
         /**
         *  CHECK IF THE going POLYLINE EXISTS IF NOT exists THEN MAKE NEW ONE else just return the result
         **/
         if($trajects->getFirst()->polyline_going == "")
         {
/*             echo "no polyline make it now"; */
            $trajectGoing = TrjctGngCrdnts::find(array(
                "conditions" => "trjct = ?1",
                "bind" => array(1=>$trajectID),
                "columns" => array("lttd,lngtd"),
                "order" => "sqnc asc",
                "hydration" => Resultset::HYDRATE_OBJECTS
            ));
            
            if($trajectGoing->count() != 0)
            {
                $goingPolyline = Polyline::Encode($trajectGoing->toArray());
                $dataGoing["status"] = true;
                $dataGoing["polyline"] = $goingPolyline;
                $dataGoing["message"] = "";
            }else
            {
                $dataGoing["status"] = false;
                $dataGoing["polyline"] = "";
                $dataGoing["message"] = "Data not available";
            }
            //$data["going"] = array($dataGoing);
            
             /**
             *  UPDATE THE TRAJECT TABLE
             */
             
             if($dataGoing["status"])
             {
                $trajectTable = trjcts::findFirst("id = ".$trajectID);
                 $trajectTable->polyline_going = $goingPolyline;
                 $result = $trajectTable->update();
                 
                 if($result)
                 {
                    $dataGoing["status_save"] = true;
                 }else
                 {
                    $dataGoing["status_save"] = false;
                 }
             }
             
             
         }else
         {
            $dataGoing["status"] = true;
            $dataGoing["polyline"] = $trajects->getFirst()->polyline_going;
            $dataGoing["message"] = "";
         }
         
         $data["going"] = $dataGoing;
         
          /**
         *  CHECK IF THE coming POLYLINE EXISTS IF NOT exists THEN MAKE NEW ONE else just return the result
         **/
         if($trajects->getFirst()->polyline_coming == "")
         { 
            $trajectComing = TrjctCmngCrdnts::find(array(
                "conditions" => "trjct = ?1",
                "bind" => array(1=>$trajectID),
                "columns" => array("lttd,lngtd"),
                "order" => "sqnc asc",
                "hydration" => Resultset::HYDRATE_OBJECTS
            ));
            
            if($trajectComing->count()!= 0)
            {
                $comingPolyline = Polyline::Encode($trajectComing->toArray());
                $dataComing["status"] = true;
                $dataComing["polyline"] = $comingPolyline;
                $dataComing["message"] = "";
            }else
            {
                $dataComing["status"] = false;                
                $dataComing["polyline"] = "";
                $dataComing["message"] = "Data not available";
            }
            
             /**
             *  UPDATE THE TRAJECT TABLE
             */
             
             if($dataComing["status"])
             {
                 $trajectTable = trjcts::findFirst("id = ".$trajectID);
                 $trajectTable->polyline_coming = $comingPolyline;
                 $result = $trajectTable->update();
                 
                 if($result)
                 {
                    $dataComing["status_save"] = true;
                 }else
                 {
                    $dataComing["status_save"] = false;
                 }

             }
        }else
        {
            $dataComing["status"] = true;
            $dataComing["message"] = "";
            $dataComing["polyline"] = $trajects->getFirst()->polyline_coming;
        }
        $data["coming"] = $dataComing; 
         echo json_encode($data);
    }
}