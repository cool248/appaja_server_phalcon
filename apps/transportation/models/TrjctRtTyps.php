<?php

namespace Appaja\API\Transportation\Models ;

use Phalcon\Mvc\Model, 
    Appaja\API\Transportation\Models\TrjctRtTypDtls ;

class TrjctRtTyps extends Model
{
    
    public function initialize()
    {
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        $this->hasMany( 'id', 'TrjctRtTypDtls', 'typ' ) ;
    }
    
}

