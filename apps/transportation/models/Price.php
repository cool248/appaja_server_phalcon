<?php

namespace Appaja\API\Transportation\Models ;

use Phalcon\Mvc\Model, 
    Appaja\API\Stop\Models\Stps,
    Appaja\API\Transportation\Models\Trjcts ;

class Price extends Model
{
    
    /**
     * Initialize
     */
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships 
        $this->hasOne( 'id_traject', 'Trjcts', 'id' ) ;
    }
    
}

