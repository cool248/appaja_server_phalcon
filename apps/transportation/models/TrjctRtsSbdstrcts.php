<?php

namespace Appaja\API\Transportation\Models ;

use Phalcon\Mvc\Model, 
 Appaja\API\Location\Models\Sbdstrcts, 
 Appaja\API\Transportation\Models\TrjctRts ;

class TrjctRtsSbdstrcts extends Model
{
    
    public function initialize()
    {
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        $this->belongsTo( 'rt', 'TrjctRts', 'id' ) ;
        $this->belongsTo( 'sbdstrct', 'Sbdstrcts', 'id' ) ;
    }
    
}

