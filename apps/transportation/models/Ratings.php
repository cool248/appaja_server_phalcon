<?php

namespace Appaja\API\Transportation\Models ;

use Phalcon\Mvc\Model,
    Appaja\API\User\Models\Users,
    Phalcon\Mvc\Model\Behavior\Timestampable,
    Appaja\API\Transportation\Models\Trjcts;

class Ratings extends Model
{
    
    public $id ;
    
    /**
     * Initialize
     */
    public function initialize() 
    {
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships
        $this->belongsTo('user_id','Users','id');
        $this->belongsTo('traject_id','Trjcts','id');
        
        date_default_timezone_set("Asia/Bangkok");
         $this->addBehavior(new Timestampable(array(
            'beforeValidationOnCreate' => array(
                'field' => 'created_at',
                'format' => 'Y-m-d H:i:s'
            )
        )));
        
    }
    
}

