<?php

namespace Appaja\API\Transportation\Models ;

use Phalcon\Mvc\Model, 
    Appaja\API\Language\Models\Lnggs, 
    Appaja\API\Transportation\Models\TrnsprttnTyps ;

class TrnsprttnTypDtls extends Model
{
    
    /**
     * Initialize
     */
    public function initialize()
    {
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships
        $this->belongsTo( 'typ', 'TrnsprttnTyps', 'id' ) ;
        $this->belongsTo( 'lngg', 'Lnggs', 'id' ) ;
    }
    
}