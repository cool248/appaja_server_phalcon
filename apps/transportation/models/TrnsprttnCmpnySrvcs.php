<?php

namespace Appaja\API\Transportation\Models ;

use Phalcon\Mvc\Model, 
    Appaja\API\Transportation\Models\TrnsprttnCmpns, 
    Appaja\API\Transportation\Models\TrnsprttnTyps ;

class TrnsprttnCmpnySrvcs extends Model
{
    
    /**
     * Initialize
     */
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships
        $this->belongsTo( 'cmpny', 'TrnsprttnCmpns', 'id' ) ;
        $this->belongsTo( 'typ', 'TrnsprttnTyps', 'id' ) ;
    }
    
}

