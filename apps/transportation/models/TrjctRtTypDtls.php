<?php

namespace Appaja\API\Transportation\Models ;

use Phalcon\Mvc\Model, 
 Appaja\API\Language\Models\Lnggs, 
 Appaja\API\Transportation\Models\TrjctRtTyps ;

class TrjctRtTypDtls extends Model
{
    
    public function initialize()
    {
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        $this->belongsTo( 'typ', 'TrjctRtTyps', 'id' ) ;
        $this->belongsTo( 'lngg', 'Lnggs', 'id' ) ;
    }
    
}

