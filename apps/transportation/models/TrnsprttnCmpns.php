<?php

namespace Appaja\API\Transportation\Models ;

use Phalcon\Mvc\Model, 
    Appaja\API\Transportation\Models\Tr;

class TrnsprttnCmpns extends Model
{
    
    /**
     * Initialize
     */
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationship
        $this->hasMany( 'id', 'TrnsprttnCmpnyDtls', 'cmpny' ) ;
        $this->hasMany( 'id', 'TrnsprttnCmpnySrvcs', 'cmpny' ) ;
    }
    
}

