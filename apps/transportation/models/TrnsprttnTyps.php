<?php

namespace Appaja\API\Transportation\Models ;

use Phalcon\Mvc\Model ;

class TrnsprttnTyps extends Model
{
    
    /**
     * Initialize
     */
    public function initialize() 
    {
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships
        $this->hasMany( 'id', 'TrnsprttnCmpnySrvcs', 'typ' ) ;
        $this->hasMany( 'id', 'TrnsprttnTypDtls', 'typ' ) ;
    }
    
}   