<?php

namespace Appaja\API\Transportation\Models ;

use Phalcon\Mvc\Model, 
    Appaja\API\Transportation\Models\TrjctRtsArs, 
    Appaja\API\Transportation\Models\Trjcts ;

class TrjctRts extends Model
{
    
    public function initialize()
    {   
        \Appaja\API\Location\Models\Cntrs::find() ;
        
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        $this->hasMany( 'id', 'TrjctRtsArs', 'rt' ) ;
        $this->belongsTo( 'trjct', 'Trjcts', 'id' ) ;
    }
    
}

