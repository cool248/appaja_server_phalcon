<?php

namespace Appaja\API\Transportation\Models ;

use Phalcon\Mvc\Model ;

class TrjctCmngCrdnts extends Model
{
    
    /**
     * Initialize
     */
    public function initialize()
    {
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships 
        $this->belongsTo( 'trjct', 'Lns', 'id' ) ;
    }
    
}