<?php

namespace Appaja\API\Transportation\Models ;

use Phalcon\Mvc\Model, 
    Appaja\API\Language\Models\Lnggs, 
    Appaja\API\Transportation\Models\TrnsprttnCmpns ;

class TrnsprttnCmpnyDtls extends Model 
{
    
    /**
     * Initialize
     */
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships
        $this->belongsTo( 'cmpny', 'TrnsprttnCmpns', 'id' ) ;
        $this->belongsTo( 'lngg', 'Lnggs', 'id' ) ;
    }
    
}

