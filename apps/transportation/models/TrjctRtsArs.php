<?php

namespace Appaja\API\Transportation\Models ;

use Phalcon\Mvc\Model, 
    Appaja\API\Location\Models\Ars, 
    Appaja\API\Transportation\Models\TrjctRts ;

class TrjctRtsArs extends Model
{
    
    public function initialize()
    {
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        $this->belongsTo( 'ar', 'Ars', 'id' ) ;
        $this->belongsTo( 'rt', 'TrjctRts', 'id' ) ;
    }
    
}

