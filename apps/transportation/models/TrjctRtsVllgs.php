<?php

namespace Appaja\API\Transportation\Models ;

use Phalcon\Mvc\Model, 
 Appaja\API\Location\Models\Vllgs, 
 Appaja\API\Transportation\Models\TrjctRts ;

class TrjctRtsVllgs extends Model
{
    
    public function initialize()
    {   
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        $this->belongsTo( 'rt', 'TrjctRts', 'id' ) ;
        $this->belongsTo( 'vllg', 'Vllgs', 'id' ) ;
    }
    
}

