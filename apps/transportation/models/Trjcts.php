<?php

namespace Appaja\API\Transportation\Models ;

use Phalcon\Mvc\Model, 
    Appaja\API\Transportation\Models\TrjctGngCrdnts, 
    Appaja\API\Transportation\Models\TrjctCmngCrdnts,
    Appaja\API\reportTransportation\Models\UserReportsTraject,
    Appaja\API\Transportation\Models\Ratings ;

class Trjcts extends Model
{
    
    public $id ;
    
    /**
     * Initialize
     */
    public function initialize() 
    {
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships
        $this->hasMany( 'id', 'LnGngCrdnts', 'trjct' ) ;
        $this->hasMany( 'id', 'LnCmngCrdnts', 'trjct' ) ;
        $this->hasMany('id','UserReportsTraject','traject_id');
        $this->hasMany('id','Ratings','traject_id');

        $this->hasOne('id','Price','id_traject');
    }
    
}

