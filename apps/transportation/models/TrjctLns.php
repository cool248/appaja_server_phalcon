<?php

namespace Appaja\API\Transportation\Models ;

use Phalcon\Mvc\Model, 
    Appaja\API\Stop\Models\Stps,
    Appaja\API\Transportation\Models\Trjcts ;

class TrjctLns extends Model
{
    
    /**
     * Initialize
     */
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships 
        $this->belongsTo( 'trjct', 'Trjcts', 'id' ) ;
        $this->belongsTo( 'stp_frm', 'Stps', 'id' ) ;
        $this->belongsTo( 'stp_t', 'Stps', 'id' ) ;
    }
    
}

