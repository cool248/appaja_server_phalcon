<?php

namespace Appaja\API\Transportation\Models ;

use Phalcon\Mvc\Model ;

class TrjctGngCrdnts extends Model
{
    
    /**
     * Initialize 
     */
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        // Define relationships 
        $this->belongsTo( 'trjct', 'Lns', 'id' ) ;
    }
    
}