<?php

namespace Appaja\API\Module\Models ;

use Phalcon\Mvc\Model ;

class MdlTbls extends Model 
{
    
    /**
     * Initialize 
     */
    public function initialize()
    {
        $this->setConnectionService( 'PostgreSQL' ) ;
    }
    
}
