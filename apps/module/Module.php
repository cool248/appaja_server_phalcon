<?php

namespace Appaja\API\Module ;

use Phalcon\Loader, 
    Phalcon\Mvc\Dispatcher, 
    Phalcon\Mvc\ModuleDefinitionInterface, 
    Phalcon\Mvc\View ;

class Module implements ModuleDefinitionInterface
{
    
    /**
     * Register a specific autoloader for the module
     */
    public function registerAutoloaders()
    {
        $loader = new Loader() ;
        $loader->registerNamespaces( array( 
            'Appaja\API\Module\Controllers' => '../apps/module/controllers/', 
            'Appaja\API\Module\Models' => '../apps/module/models/' )
        )->register() ;
    }
    
    /**
     * Register services
     */
    public function registerServices( $di ) 
    {
        // Set a dispatcher
        $di->set( 'dispatcher', function() {
            $dispatcher = new Dispatcher() ;
            $dispatcher->setDefaultNamespace( 'Appaja\API\Module\Controllers' ) ;
            
            return $dispatcher ;
        } ) ;
        
        $di->set( 'view', function() {
            $view = new View() ;
            $view->setViewsDir( '../apps/module/views' ) ;
            
            return $view ;
        } ) ;
    }
    
}
    

