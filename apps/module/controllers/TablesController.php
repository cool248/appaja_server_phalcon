<?php

namespace Appaja\API\Module\Controllers ;

use Phalcon\Mvc\Controller, 
    Appaja\API\Module\Models\MdlTbls ;

class TablesController extends Controller
{
    
    public function enquireAction()
    {
        $mdls = array() ;
        
        $this->mdls_st( $mdls ) ;
        
        echo json_encode( array( 'errr' => 0, 'dt' => array( 'mdls' => $mdls ) ) ) ;
    }
    
    private function mdls_st( &$mdls )
    {
        $mdls_rslt_st = MdlTbls::find( array( 
            'cache' => array( 'key' => 'mdl_tbls-enqr' ) ) ) ;
        
        $ii = 0 ;
        while( $mdls_rslt_st->valid() )
        {
            $tp_rcrd = $mdls_rslt_st->current() ;
            $mdls[ $ii ][ 'vrsn' ] = $tp_rcrd->vrsn ;
            $mdls[ $ii ][ 'updtd_lst' ] = $tp_rcrd->updtd_lst ;
            $mdls[ $ii ][ 'cgnznc' ] = $tp_rcrd->cgnznc ;
            
            $ii++ ;
            $mdls_rslt_st->next() ;
        }
    }
    
}
