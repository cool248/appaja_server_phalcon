<?php
namespace Appaja\API\reportTransportation\Models ;

use Phalcon\Mvc\Model,
    Appaja\API\User\Models\Users,
    Phalcon\Mvc\Model\Behavior\Timestampable,
    Appaja\API\Transportation\Models\Trjcts;


class UserReportsTraject extends Model
{
     /**
     * Initialize
     */
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
        $this->belongsTo('report_detail_id','ReportDetail','id');
        $this->belongsTo('traject_id','Trjcts','id');
        $this->belongsTo('user_id','Users','id');
        
        
        date_default_timezone_set("Asia/Bangkok");
         $this->addBehavior(new Timestampable(array(
            'beforeValidationOnCreate' => array(
                'field' => 'created_at',
                'format' => 'Y-m-d H:i:s'
            )
        )));
        
        
    }   
}