<?php
namespace Appaja\API\reportTransportation\Models ;

use Phalcon\Mvc\Model;

class ReportDetail extends Model
{
    
     /**
     * Initialize
     */
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
        
        $this->hasMany('id','UserReportsTraject','report_detail_id');
        
    }
    
    
}