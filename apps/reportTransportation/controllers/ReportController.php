<?php 

namespace Appaja\API\reportTransportation\Controllers ;

use Phalcon\Db\Column, 
    Phalcon\Mvc\Controller, 
    Phalcon\Mvc\Model\Resultset,
    Appaja\API\reportTransportation\Models\ReportDetail,
    Appaja\API\reportTransportation\Models\UserReportsTraject;
    
    class ReportController extends Controller
    {
        public function indexAction()
        {
            echo "test";
        }
        
        /**
        *   return json ARRAY();
        */
        public function getAllAction()
        {
            $data['data'] = ReportDetail::find()->toArray();
            echo json_encode($data);
        }
        
        
        /**
        *   4 parameters
        *   
        *   1. user_id
        *   2. report_detail_id
        *   3. traject_id
        *   4. comments
        **/
        public function sendReportAction()
        {
            
            $data = array();
            $data['status'] = false;
            
            $checkArrayUserId = $this->checkQuery('user_id',"int");
           
            $checkArrayReportDetailId = $this->checkQuery('report_detail_id',"int");
            
            $checkArrayTrajectId = $this->checkQuery('traject_id',"int");
            
            $checkArrayComment = $this->checkQuery('comment',"string");
            
            $timestamp = $this->request->getQuery('timestamp');
            
            
            $report = new UserReportsTraject();
            $report->user_id = $checkArrayUserId['value'];
            $report->report_detail_id =  $checkArrayReportDetailId['value'];
            $report->traject_id = $checkArrayTrajectId['value'];
            $report->comments = $checkArrayComment['value'];
            $report->save();
            
            $data['status'] = true;
            $data['message'] = "Thank you for reporting";
            
            echo json_encode($data);
            
                        
        }
        
        
        
        
        public function checkQuery($var,$type)
        {
            $data = array();
            $data['status'] = false;
            $value = null;
            
            if($this->request->hasQuery($var))
            {
                $value = $this->request->getQuery($var,$type);
                if(empty($value))
                {
                    $data['message'] = $var." is empty";
                }else
                {
                    $data['status'] = true;
                    $data['message'] = "Good";
                    $data['value'] = $value;
                }
                
            }else
            {
                $data['message'] = $var." Not Avalaible.";
            }
            
            
            if(!$data['status'])
            {
                echo json_encode($data);
                exit();
            }
            return $data;
        }
        
        
    }