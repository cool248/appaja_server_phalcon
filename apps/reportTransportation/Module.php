<?php

namespace Appaja\API\reportTransportation ;

use Phalcon\Loader,
    Phalcon\Mvc\Dispatcher, 
    Phalcon\Mvc\ModuleDefinitionInterface, 
    Phalcon\Mvc\View,
    Phalcon\Mvc\Model\Manager ;

class Module implements ModuleDefinitionInterface
{
    
    /**
     * Register a specific autoloader for the module
     */
    public function registerAutoloaders()
    {
        $loader = new Loader() ;
        $loader->registerNamespaces( array(
            'Appaja\API\reportTransportation\Controllers' => '../apps/reportTransportation/controllers/', 
            'Appaja\API\reportTransportation\Library' => '../apps/reportTransportation/library/', 
            'Appaja\API\reportTransportation\Models' => '../apps/reportTransportation/models/'
        ) )->register() ;
    }
    
    /**
     * Register services
     */
    public function registerServices( $di )
    {
        $di->set( 'dispatcher', function() {
            $dispatcher = new Dispatcher() ;
            $dispatcher->setDefaultNamespace( 'Appaja\API\reportTransportation\Controllers' ) ;
            
            return $dispatcher ;
        } ) ;
        
        $di->set( 'view', function() {
            $view = new View() ;
            $view->setViewsDir( '../apps/reportTransportation/views' ) ;
            
            return $view ;
        } ) ;
        
        $di->set('modelsManager', function() {
      return new Manager();
 });
    }
    
}

