<?php
use Phalcon\Mvc\Model;

class CronJob extends Model 
{
    /**
     * Initialize
     */
    public function initialize()
    {
        // Set connection service
        $this->setConnectionService( 'PostgreSQL' ) ;
    }
}