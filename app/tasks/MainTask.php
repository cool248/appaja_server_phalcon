<?php 

use Phalcon\Db\Column, 
    Phalcon\Mvc\Controller;

class MainTask extends \Phalcon\CLI\Task
{

    public function mainAction() 
    {
         echo "\n my name is mansoor and i am an newbie but cli of phalcon is awesome dude \n";
    }
       
   /**
    * @param array $params
    */
    public function testAction() 
    {        
       $result = CronJob::find();
        $cronJob = new CronJob();
        $text = "test".(count($result)+1);
        $cronJob->some_text = $text;
        $cronJob->save();
        $result = CronJob::find();
    }
    
    public function crowdDropOffAction()
    {   
        /**
        *   GET ALL THE STOPS WITH reduced_value not equal t0 0
        */

        $result = StpRprts::find(array(
            "conditions" => 'reduced_value != ?1',
            "bind" => array(1 => '0'),
            "order" => "id asc"
        ));
        
        $accumulatedStopValue = array();
        foreach ($result as $r) 
        {
            $result = CrowdnessMetrics ::reduceMetrics($r->reduced_value);
            if($result['metric'] != 0)
            {
                 $accumulatedStopValue[$r->stp][] =  $result['metric'];
            }
           
            /**
            *   update server
            */
            $r->reduced_value = $result['metric'];
            $r->update();
        }
        
        $addMetrics = 0;
        /**
        *    STOPS
        */
        foreach($accumulatedStopValue as $k => $value)
        {
           $addMetrics = 0;
           /**
           *    calculate all the value
           */
           foreach($value as $v)
           {
               $addMetrics = $addMetrics + $v;
           }
           
           /***
           *    find the crowdness metrics
           **/
           $endValueMetric = CrowdnessMetrics::crowdness(round($addMetrics / count($value)));
          
           
           $currentStop = Stps::findFirst(array(
            "conditions" => 'id = ?1',
            "bind" => array(1 => $k)
           ));
           
           $currentStop->crwdnss = $endValueMetric;
           $currentStop->update();
        }
        
        

    }
        
}