<?php

class CrowdnessMetrics
{    
    public static function reduceMetrics($crowdednessMetric)
    {
        $arr = null;
        switch($crowdednessMetric)
        {
            case ($crowdednessMetric >= 1 && $crowdednessMetric <= 30) :
                $arr['status'] = 1;
                $arr['metric'] = $crowdednessMetric - 10;
                break;
            
            case ($crowdednessMetric >= 31 && $crowdednessMetric <= 60) :
                $arr['status'] = 2;
                $arr['metric'] = $crowdednessMetric - 5;
                break;
            
            case ($crowdednessMetric >= 61 && $crowdednessMetric <= 100) :
                $arr['status'] = 3;
                $arr['metric'] = $crowdednessMetric - 2;
                break;
            
            case ($crowdednessMetric == 0):
                $arr['status'] = 0;
                $arr['metric'] =  0;
                break;
            
            default:
                $arr['status'] = 0;
                $arr['metric'] = 0;
                break;
        }
        
        
        /**
        *   CHECK IF THE reducedMetric less than 0
        */
        if($arr['metric'] <=0)
        {
            $arr['metric'] = 0;
            $arr['status'] = 0;
        }
        
        return $arr;
    }
    
    
    
    public static function crowdness($crowdednessMetric)
    {
        $arr = null;
        switch($crowdednessMetric)
        {
            case ($crowdednessMetric >= 1 && $crowdednessMetric <= 30) :
                $arr['status'] = 1;
                break;
            
            case ($crowdednessMetric >= 31 && $crowdednessMetric <= 60) :
                $arr['status'] = 2;
                break;
            
            case ($crowdednessMetric >= 61 && $crowdednessMetric <= 100) :
                $arr['status'] = 3;
                break;
            
            case ($crowdednessMetric == 0):
                $arr['status'] = 0;
                break;
            
            default:
                $arr['status'] = 0;
                break;
        }        
        return $arr['status'];
    }
}