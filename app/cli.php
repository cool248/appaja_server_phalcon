<?php

 use Phalcon\DI\FactoryDefault\CLI as CliDI,
     Phalcon\CLI\Console as ConsoleApp,
     Phalcon\Config, 
     Phalcon\Config\Adapter\Ini;

 define('VERSION', '1.0.0');

 //Using the CLI factory default services container
 $di = new CliDI();

 // Define path to application directory
 defined('APPLICATION_PATH')
 || define('APPLICATION_PATH', realpath(dirname(__FILE__)));



//Read the configuration
	$config = new Config() ;
    $config->merge( new Ini( __DIR__.'/../common/config/databases.ini' ) ) ;

 /**
  * Register the autoloader and tell it to register the tasks directory
  */

 $loader = new \Phalcon\Loader();
 $loader->registerDirs(
     array(
         APPLICATION_PATH . '/tasks',
         APPLICATION_PATH . '/models',
         APPLICATION_PATH . '/library'
     )
 );
 $loader->register();

 
	/**
	 * Database connection is created based in the parameters defined in the configuration file
	 */
    include __DIR__.'/../common/config/services.php' ;



 //Create a console application
 $console = new ConsoleApp();
 $console->setDI($di);

 
 
 /**
 * Process the console arguments
 */
 $arguments = array();
 foreach($argv as $k => $arg) {
     if($k == 1) {
         $arguments['task'] = $arg;
     } elseif($k == 2) {
         $arguments['action'] = $arg;
     } elseif($k >= 3) {
        $arguments['params'][] = $arg;
     }
 }

 // define global constants for the current task and action
 define('CURRENT_TASK', (isset($argv[1]) ? $argv[1] : null));
 define('CURRENT_ACTION', (isset($argv[2]) ? $argv[2] : null));


$di->setShared('console', $console);
 try {
     // handle incoming arguments
     $console->handle($arguments);
 }
 catch (\Phalcon\Exception $e) {
     echo $e->getMessage();
     exit(255);
 }